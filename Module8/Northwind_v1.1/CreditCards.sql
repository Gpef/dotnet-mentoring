﻿CREATE TABLE [dbo].[CreditCards]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
	[EmployeeID] INT NULL,
    [Number] NCHAR(16) NULL, 
    [Expires] DATETIME NULL, 
    [CardHolder] VARCHAR(50) NULL, 
	CONSTRAINT [FK_Employee] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employees]([EmployeeID])
)
