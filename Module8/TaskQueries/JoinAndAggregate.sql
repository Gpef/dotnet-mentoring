﻿/* Task 2.1.1 */
SELECT SUM(UnitPrice - UnitPrice * Discount) as "Totals"
FROM dbo.[Order Details];

/* Task 2.1.2 */	
SELECT COUNT(*) - COUNT(ShippedDate)
FROM dbo.Orders;

/* Task 2.1.3*/		
SELECT COUNT(DISTINCT CustomerId)
FROM dbo.Orders;

/* Task 2.2.1*/	
/* Pure right task: */
SELECT year(OrderDate) as "Order Year", COUNT(OrderId) as "Orders Count"
FROM dbo.Orders
GROUP BY year(OrderDate);

/*Query for checking: */
SELECT 
	year(OrderDate) as "Order Year", 
	COUNT(OrderId) as "Orders Count", 
	(SELECT COUNT(*) FROM dbo.Orders) as "All"
FROM dbo.Orders
GROUP BY year(OrderDate);
	
/* Task 2.2.2*/	
SELECT 
	COUNT(*) as Amount,
	(SELECT TOP 1 CONCAT(LastName,' ', FirstName) 
		FROM dbo.Employees as e 
		WHERE o.EmployeeID = e.EmployeeID) as Seller
FROM dbo.Orders as o
GROUP BY EmployeeID
ORDER BY Amount DESC;

/*With Join*/
SELECT
	COUNT(*) as Amount,
	CONCAT(e.LastName,' ', e.FirstName) as Seller
FROM dbo.Orders as o 
INNER JOIN dbo.Employees as e ON o.EmployeeID = e.EmployeeID
GROUP BY e.EmployeeID, CONCAT(e.LastName,' ', e.FirstName) /*Need help*/
ORDER BY Amount DESC;

/* Task 2.2.3*/		
SELECT 
	EmployeeID as Seller,
	CustomerID as Customer,
	COUNT(*) as Amount
FROM dbo.Orders
WHERE year(OrderDate) = 1998
GROUP BY EmployeeID, CustomerID
ORDER BY Seller;

/* Task 2.2.4*/	
/*On Q&A session it was changed to 'show numbers of customers from the same sity where more than 1*/
SELECT COUNT(*), City
FROM dbo.Customers
GROUP BY City
HAVING COUNT(*) > 1;

/* Task 2.2.5*/	
SELECT CompanyName, City
FROM dbo.Customers
GROUP BY City, CompanyName
ORDER BY City;/*TODO*/

/* Task 2.2.6*/		
SELECT
	CONCAT(e.FirstName, ' ', e.LastName) as Seller,
	CASE 
		WHEN (e.ReportsTo IS NULL) THEN '-'
		ELSE CONVERT(varchar, (CONCAT(e_rep.FirstName, ' ', e_rep.LastName)))
	END as "Reports to"
FROM dbo.Employees e
LEFT JOIN dbo.Employees as e_rep ON e.ReportsTo = e_rep.EmployeeID
ORDER BY e.EmployeeID;

/* Task 2.3.1*/	
SELECT 
	CONCAT(e.FirstName, ' ', e.LastName) as Seller,
	r.RegionDescription as Region
FROM dbo.Employees as e
INNER JOIN dbo.EmployeeTerritories as et ON e.EmployeeID = et.EmployeeID
INNER JOIN dbo.Territories as t ON et.TerritoryID = t.TerritoryID
INNER JOIN dbo.Region as r ON t.RegionID = r.RegionID
WHERE r.RegionDescription = 'Western';

/* Task 2.3.2*/	
SELECT 
	c.CompanyName,
	COUNT(o.OrderID) as OrdersCount
FROM dbo.Customers as c
LEFT JOIN dbo.Orders as o ON c.CustomerID = o.CustomerID
GROUP BY c.CustomerID, c.CompanyName
ORDER BY OrdersCount DESC;

/* Task 2.4.1*/	
SELECT s.CompanyName
FROM dbo.Suppliers as s
WHERE s.SupplierID IN (SELECT SupplierID FROM dbo.Products WHERE UnitsInStock = 0);

/* Task 2.4.2*/	
SELECT CONCAT(e.FirstName, ' ', e.LastName) as Seller
FROM dbo.Employees as e
WHERE e.EmployeeID IN 
	(SELECT EmployeeID 
	 FROM dbo.Orders 
	 GROUP BY EmployeeID
	 HAVING COUNT(OrderID) > 150);

/* Task 2.4.3*/	
SELECT CompanyName
FROM dbo.Customers as c
WHERE NOT EXISTS (SELECT * FROM dbo.Orders as o WHERE o.CustomerID = c.CustomerID);