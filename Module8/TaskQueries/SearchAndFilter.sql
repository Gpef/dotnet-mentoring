﻿/* Task 1.1.1 */
SELECT OrderID, ShippedDate, ShipVia 
FROM dbo.Orders 
WHERE ShipVia >= 2 AND ShippedDate >= Convert(datetime, '1998-05-06' );

/* Task 1.1.2 */
SELECT 
OrderID,
CASE 
	WHEN (ShippedDate IS NULL) THEN 'Not Shipped'
	END AS ShippedDate
FROM dbo.Orders
WHERE ShippedDate IS NULL;

/* Task 1.1.3 */
SELECT 
OrderID AS "Order Number", 
CASE 
	WHEN (ShippedDate IS NULL) THEN 'Not Shipped'
	ELSE CONVERT(varchar, ShippedDate)
	END AS "Shipped Date"
FROM dbo.Orders 
WHERE ShippedDate > Convert(datetime, '1998-05-06' ) OR ShippedDate IS NULL;

/* Task 1.2.1 */
SELECT ContactName, Country 
FROM dbo.Customers
WHERE Country IN ('USA', 'Canada')
ORDER BY ContactName, Country;

/* Task 1.2.2 */
SELECT ContactName, Country 
FROM dbo.Customers
WHERE Country NOT IN ('USA', 'Canada')
ORDER BY ContactName;

/* Task 1.2.3 */
SELECT DISTINCT Country 
FROM dbo.Customers
ORDER BY Country DESC;

/* Task 1.3.1 */
SELECT DISTINCT OrderID
FROM dbo.[Order Details]
WHERE Quantity BETWEEN 4 AND 10;

/* Task 1.3.2 */
SELECT CustomerID, Country
FROM dbo.Customers
WHERE LEFT(Country, 1) BETWEEN 'b' AND 'g'
ORDER BY Country;

/* Task 1.3.3 */
SELECT CustomerID, Country
FROM dbo.Customers
WHERE Country LIKE '[b-g]%'
ORDER BY Country;

/* Task 1.4.1 */
SELECT ProductName
FROM dbo.Products
WHERE ProductName like '%cho_olade%';