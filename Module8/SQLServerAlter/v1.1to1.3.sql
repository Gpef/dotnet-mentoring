/*
   Tuesday, November 27, 20183:08:40 AM
   User: 
   Server: EPBYMINW4846
   Database: Northwind
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
IF NOT EXISTS (SELECT 'table does not exist' FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'Regions' AND TABLE_SCHEMA = 'dbo')
	BEGIN
		USE "Northwind";
		EXECUTE sp_rename N'dbo.Region', N'Regions';
		ALTER TABLE dbo.Regions SET (LOCK_ESCALATION = TABLE);
	END
GO
COMMIT
