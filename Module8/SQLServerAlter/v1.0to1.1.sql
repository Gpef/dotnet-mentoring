USE [Northwind]
GO

ALTER TABLE [dbo].[CreditCard] DROP CONSTRAINT [FK_Employee]
GO

/****** Object:  Table [dbo].[CreditCard]    Script Date: 11/27/2018 3:01:41 AM ******/
DROP TABLE [dbo].[CreditCard]
GO

/****** Object:  Table [dbo].[CreditCard]    Script Date: 11/27/2018 3:01:41 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CreditCard](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[CardNumber] [char](16) NULL,
	[Expires] [date] NULL,
	[CardHolder] [varchar](50) NULL,
 CONSTRAINT [PK_CreditCard] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CreditCard]  WITH CHECK ADD  CONSTRAINT [FK_Employee] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Employees] ([EmployeeID])
GO

ALTER TABLE [dbo].[CreditCard] CHECK CONSTRAINT [FK_Employee]
GO


