﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions;
using Module2.CustomEventArgs;

namespace Module2
{
    /// <summary>
    /// Can go through files and directories and filter them based on specified rule or
    /// overriden in events (it returns in <see cref="EventArgs"/>).
    /// </summary>
    public class FileSystemVisitor
    {
        /// <summary>
        /// Template for method that can decide whether file/directory
        /// is needed to be included in the list returned or not.
        /// </summary>
        /// <param name="foundEntry"></param>
        public delegate bool FilteringMethod(FileSystemInfoBase foundEntry);

        /// <summary>
        /// Event called if filtering was started successfully.
        /// </summary>
        public event EventHandler<FilteringStateEventArgs> FilteringStartedEvent;

        /// <summary>
        /// Event called if filtering was finished (it can be force aborted or finished by itself).
        /// </summary>
        public event EventHandler<FilteringStateEventArgs> FilteringFinishedEvent;

        /// <summary>
        /// Event called when file is found in one of child directories.
        /// </summary>
        public event EventHandler<FoundEventArgs> FileFoundEvent;

        /// <summary>
        /// Event called when file found earlier went through <see cref="FilteringMethod"/> and
        /// it is known it needed to be skipped or not.
        /// </summary>
        public event EventHandler<FilterEventArgs> FileFilteredEvent;

        /// <summary>
        /// Event called when directory is found in one of child directories.
        /// </summary>
        public event EventHandler<FoundEventArgs> DirectoryFoundEvent;

        /// <summary>
        /// Event called when directory found earlier went through <see cref="FilteringMethod"/> and
        /// it is known it needed to be skipped or not.
        /// </summary>
        public event EventHandler<FilterEventArgs> DirectoryFilteredEvent;

        /// <summary>
        /// Specified file system to filter files and directories. Obligatory for testing with Mocked FileSystem.
        /// </summary>
        private readonly IFileSystem _fileSystem;

        /// <summary>
        /// Method specified by <see cref="FilteringMethod"/> delegate. 
        /// Decides if file needed to be skipped or not.
        /// </summary>
        private readonly FilteringMethod _filteringMethodMethod;

        public FileSystemVisitor(IFileSystem fileSystem)
        {
            _fileSystem = fileSystem;
        }

        public FileSystemVisitor(IFileSystem fileSystem, FilteringMethod filteringMethodMethod)
        {
            _fileSystem = fileSystem;
            _filteringMethodMethod = filteringMethodMethod;
        }

        private IEnumerable<string> SearchSubDirectories(string directory) =>
            SearchFor(_fileSystem.Directory.GetDirectories, directory);

        private IEnumerable<string> SearchFiles(string directory) =>
            SearchFor(_fileSystem.Directory.GetFiles, directory);

        private IEnumerable<string> SearchFor(Func<string, string[]> searchFunction, string pathToSearch)
        {
            var itemsFound = new string[0];
            try
            {
                itemsFound = searchFunction(pathToSearch);
            }
            catch (Exception e) when (e is UnauthorizedAccessException || e is DirectoryNotFoundException)
            {
                Console.WriteLine($"Not able to search in directory \"{pathToSearch}\". {e.Message}");
            }

            return itemsFound;
        }

        /// <summary>
        /// Visits <see cref="path"/> directory, searches for files in directories in it and its children
        /// and filters them based on <see cref="FilteringMethod"/> if it was specified and events using
        /// <see cref="EventArgs"/> returned after event calling.
        /// </summary>
        /// <param name="path">Root directory path for filtering start.</param>
        /// <returns>List of full names if files and directories that successfully passed filtering.</returns>
        /// <exception cref="ArgumentException">If path provided is not exists.</exception>
        public IEnumerable<string> Visit(string path)
        {
            if (!_fileSystem.Directory.Exists(path))
            {
                throw new ArgumentException($"Path \"{path}\" is not exist. Filtering will not be started.");
            }

            FilteringStartedEvent?.Invoke(this, new FilteringStateEventArgs(path, FilteringState.Started));

            foreach (var filterArgs in InternalVisit(path))
            {
                if (filterArgs == null) continue;

                if (!filterArgs.IsForceSkipNeeded && filterArgs.IsFilterPassed)
                    yield return filterArgs.ItemPath;

                if (filterArgs.IsForceAbortNeeded)
                {
                    FilteringFinishedEvent?.Invoke(this, new FilteringStateEventArgs(path, FilteringState.Aborted));
                    yield break;
                }
            }

            FilteringFinishedEvent?.Invoke(this, new FilteringStateEventArgs(path, FilteringState.Finished));
        }

        private IEnumerable<FilterEventArgs> InternalVisit(string path)
        {
            var directories = new Queue<string>();
            directories.Enqueue(path);
            while (directories.Count > 0)
            {
                var currentDirectory = directories.Dequeue();

                var subDirectories = SearchSubDirectories(currentDirectory);
                foreach (var directory in subDirectories)
                {
                    directories.Enqueue(directory);

                    var filterArgs = ProcessDirectoryFilterEvents(directory);
                    yield return filterArgs;
                }

                var currentFiles = SearchFiles(currentDirectory);
                foreach (var file in currentFiles)
                {
                    var filterArgs = ProcessFileFilterEvents(file);
                    yield return filterArgs;
                }
            }
        }

        private FilterEventArgs ProcessFilterEvents(
            string path,
            bool isDirectory,
            EventHandler<FoundEventArgs> foundEvent,
            EventHandler<FilterEventArgs> filterEvent)
        {
            var foundArgs = new FoundEventArgs(path);
            foundEvent?.Invoke(this, foundArgs);
            var filteredArgs = new FilterEventArgs(foundArgs);

            if (filteredArgs.IsForceSkipNeeded)
            {
                filteredArgs.IsFilterPassed = false;
            }
            else
            {
                filteredArgs.IsFilterPassed = _filteringMethodMethod?.Invoke(GetInfo(path, isDirectory)) ?? true;
            }

            filterEvent?.Invoke(this, filteredArgs);

            return filteredArgs;
        }

        private FilterEventArgs ProcessDirectoryFilterEvents(string directoryPath) =>
            ProcessFilterEvents(directoryPath, true, DirectoryFoundEvent, DirectoryFilteredEvent);

        private FilterEventArgs ProcessFileFilterEvents(string filePath) =>
            ProcessFilterEvents(filePath, false, FileFoundEvent, FileFilteredEvent);

        private FileSystemInfoBase GetInfo(string path, bool isDirectory)
        {
            if (isDirectory)
            {
                return new DirectoryInfoWrapper(_fileSystem, new DirectoryInfo(path));
            }
            else
            {
                return new FileInfoWrapper(_fileSystem, new FileInfo(path));
            }
        }
    }
}