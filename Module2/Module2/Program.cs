﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Module2.CustomEventArgs;

namespace Module2
{
    /// <summary>
    /// Example of using <see cref="FileSystemVisitor"/> with all events handling
    /// </summary>
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            var visitor = new FileSystemVisitor(new FileSystem(), FilteringMethod);

            visitor.FilteringStartedEvent += OnFilteringStarted;
            visitor.FilteringFinishedEvent += OnFilteringFinished;
            visitor.FileFoundEvent += OnFileFound;
            visitor.FileFilteredEvent += OnFileFiltered;
            visitor.DirectoryFoundEvent += OnDirectoryFound;
            visitor.DirectoryFilteredEvent += OnDirectoryFiltered;

            var files = visitor.Visit(@"D:\Home\Books").ToList();
        }

        private static bool FilteringMethod(FileSystemInfoBase item) => item.Name.Length < 55;

        private static void OnDirectoryFiltered(object sender, FilterEventArgs args)
        {
            Console.WriteLine($"[Filtered] Directory: \"{args.ItemPath}\". Result: {args.IsFilterPassed}");
        }

        private static void OnDirectoryFound(object sender, FoundEventArgs args)
        {
            Console.WriteLine($"[Found] Directory: \"{args.ItemPath}\".");
        }

        private static void OnFileFound(object sender, FoundEventArgs args)
        {
            Console.WriteLine($"[Found] File: \"{args.ItemPath}\".");
        }

        private static void OnFileFiltered(object sender, FilterEventArgs args)
        {
            if (args.ItemPath.EndsWith(".epub"))
            {
                args.IsForceAbortNeeded = true;
                args.IsForceSkipNeeded = true;
            }

            Console.WriteLine($"[Filtered] File \"{args.ItemPath}\". Result: {args.IsFilterPassed}");
        }

        private static void OnFilteringFinished(object sender, FilteringStateEventArgs args)
        {
            Console.WriteLine(
                $"{nameof(FileSystemVisitor)} finished filtering of \"{args.PathToFilter}\" with state {args.State}");
        }

        private static void OnFilteringStarted(object sender, FilteringStateEventArgs args)
        {
            Console.WriteLine($"{nameof(FileSystemVisitor)} starting filtering of \"{args.PathToFilter}\"");
        }
    }
}