namespace Module2.CustomEventArgs
{
    public class FilterEventArgs : FoundEventArgs
    {
        public FilterEventArgs(string itemPath) : base(itemPath)
        {
        }

        public FilterEventArgs(FoundEventArgs foundArgs) : base(foundArgs.ItemPath)
        {
            IsForceAbortNeeded = foundArgs.IsForceAbortNeeded;
            IsForceSkipNeeded = foundArgs.IsForceSkipNeeded;
        }

        public bool IsFilterPassed { get; set; } = false;
    }
}