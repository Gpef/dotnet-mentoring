using System;

namespace Module2.CustomEventArgs
{
    public enum FilteringState
    {
        Started,
        Finished,
        Aborted
    }

    public class FilteringStateEventArgs : EventArgs
    {
        public FilteringStateEventArgs(string path, FilteringState state)
        {
            PathToFilter = path;
            State = state;
        }

        public FilteringState State { get; set; }
        public string PathToFilter { get; set; }
    }
}