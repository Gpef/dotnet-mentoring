using System;

namespace Module2.CustomEventArgs
{
    public class FoundEventArgs : EventArgs
    {
        public FoundEventArgs(string itemPath)
        {
            ItemPath = itemPath;
        }

        public bool IsForceAbortNeeded { get; set; } = false;
        public bool IsForceSkipNeeded { get; set; } = false;
        
        public string ItemPath { get; }
    }
}