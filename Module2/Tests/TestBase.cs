using System.Collections.Generic;
using System.IO.Abstractions;
using System.IO.Abstractions.TestingHelpers;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public class TestBase
    {
        protected static MockFileSystem FileSystem => new MockFileSystem(new Dictionary<string, MockFileData>
        {
            {@"C:\Parent\Child1\File1.txt", new MockFileData("File1 text")},
            {@"C:\Parent\Child2\File2.pdf", new MockFileData(new byte[] {0x12, 0x34})},
            {@"C:\Parent\Child2\File3.docx", new MockFileData(new byte[] {0x12, 0x34, 0x56, 0xd2})},
            {@"C:\Parent\Child3\File4.pdf", new MockFileData(new byte[] {0x12, 0x34, 0x56})}
        });

        protected const string RootDirectory = @"C:\";

        protected static bool FilterPdfOnly(FileSystemInfoBase info) => info.Name.EndsWith(".pdf");
        protected static bool FilterFilesOnly(FileSystemInfoBase info) => info.Extension != string.Empty;
    }
}