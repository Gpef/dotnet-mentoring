using System.Collections.Generic;
using System.Linq;
using Module2;
using Module2.CustomEventArgs;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public class SkipFilteringTests : TestBase
    {
        [Test]
        public void SkipFileOnFoundEvent()
        {
            var visitor = new FileSystemVisitor(FileSystem);
            visitor.FileFoundEvent += (object sender, FoundEventArgs args) =>
            {
                var fileInfo = FileSystem.FileInfo.FromFileName(args.ItemPath);
                if (fileInfo.Length < 4)
                {
                    args.IsForceSkipNeeded = true;
                }
            };

            var filtered = visitor.Visit(RootDirectory).ToList();

            var expectedFiltered = new List<string>
            {
                @"C:\Parent",
                @"C:\Parent\Child1",
                @"C:\Parent\Child2",
                @"C:\Parent\Child3",
                @"C:\Parent\Child1\File1.txt",
                @"C:\Parent\Child2\File3.docx"
            };

            CollectionAssert.AreEqual(expectedFiltered, filtered);
        }

        [Test]
        public void SkipFileOnFilterEvent()
        {
            var visitor = new FileSystemVisitor(FileSystem);
            visitor.FileFilteredEvent += (object sender, FilterEventArgs args) =>
            {
                var fileInfo = FileSystem.FileInfo.FromFileName(args.ItemPath);
                if (fileInfo.Length < 3)
                {
                    args.IsForceSkipNeeded = true;
                }
            };

            var filtered = visitor.Visit(RootDirectory).ToList();

            var expectedFiltered = new List<string>
            {
                @"C:\Parent",
                @"C:\Parent\Child1",
                @"C:\Parent\Child2",
                @"C:\Parent\Child3",
                @"C:\Parent\Child1\File1.txt",
                @"C:\Parent\Child2\File3.docx",
                @"C:\Parent\Child3\File4.pdf"
            };

            CollectionAssert.AreEqual(expectedFiltered, filtered);
        }

        [Test]
        public void SkipDirectoryOnFoundEvent()
        {
            const string parentNameToExclude = "Parent";

            var visitor = new FileSystemVisitor(FileSystem);
            visitor.DirectoryFoundEvent += (object sender, FoundEventArgs args) =>
            {
                var directoryInfo = FileSystem.DirectoryInfo.FromDirectoryName(args.ItemPath);
                if (directoryInfo.Parent.Name == parentNameToExclude)
                {
                    args.IsForceSkipNeeded = true;
                }
            };

            var filtered = visitor.Visit(RootDirectory).ToList();

            var expectedFiltered = new List<string>
            {
                @"C:\Parent",
                @"C:\Parent\Child1\File1.txt",
                @"C:\Parent\Child2\File2.pdf",
                @"C:\Parent\Child2\File3.docx",
                @"C:\Parent\Child3\File4.pdf"
            };

            CollectionAssert.AreEqual(expectedFiltered, filtered);
        }

        [Test]
        public void SkipDirectoryOnFilterEvent()
        {
            const string parentNameToIncludeOnly = "Parent";

            var visitor = new FileSystemVisitor(FileSystem);
            visitor.DirectoryFilteredEvent += (object sender, FilterEventArgs args) =>
            {
                var directoryInfo = FileSystem.DirectoryInfo.FromDirectoryName(args.ItemPath);
                if (directoryInfo.Parent.Name != parentNameToIncludeOnly)
                {
                    args.IsForceSkipNeeded = true;
                }
            };

            var filtered = visitor.Visit(RootDirectory).ToList();

            var expectedFiltered = new List<string>
            {
                @"C:\Parent\Child1",
                @"C:\Parent\Child2",
                @"C:\Parent\Child3",
                @"C:\Parent\Child1\File1.txt",
                @"C:\Parent\Child2\File2.pdf",
                @"C:\Parent\Child2\File3.docx",
                @"C:\Parent\Child3\File4.pdf"
            };

            CollectionAssert.AreEqual(expectedFiltered, filtered);
        }
    }
}