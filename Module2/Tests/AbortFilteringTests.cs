using System.Collections.Generic;
using System.Linq;
using Module2;
using Module2.CustomEventArgs;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public class AbortFilteringTests : TestBase
    {
        [Test]
        public void AbortOnFileFound([Values(true, false)] bool includeStopFile)
        {
            const string extensionToAbort = ".docx";
            
            var visitor = new FileSystemVisitor(FileSystem);
            visitor.FileFoundEvent += (object sender, FoundEventArgs args) =>
            {
                var fileSystemInfo = FileSystem.FileInfo.FromFileName(args.ItemPath);
                if (fileSystemInfo.Extension == extensionToAbort)
                {
                    args.IsForceAbortNeeded = true;
                    args.IsForceSkipNeeded = !includeStopFile;
                }
            };

            var filtered = visitor.Visit(RootDirectory).ToList();

            var expectedFiltered = new List<string>
            {
                @"C:\Parent",
                @"C:\Parent\Child1",
                @"C:\Parent\Child2",
                @"C:\Parent\Child3",
                @"C:\Parent\Child1\File1.txt",
                @"C:\Parent\Child2\File2.pdf"
            };

            if (includeStopFile)
            {
                expectedFiltered.Add(@"C:\Parent\Child2\File3.docx");
            }

            CollectionAssert.AreEqual(expectedFiltered, filtered);
        }

        [Test]
        public void AbortOnDirectoryFound([Values(true, false)] bool includeStopDirectory)
        {
            const string folderToAbort = "Child2";
            
            var visitor = new FileSystemVisitor(FileSystem);
            visitor.DirectoryFoundEvent += (object sender, FoundEventArgs args) =>
            {
                var fileSystemInfo = FileSystem.DirectoryInfo.FromDirectoryName(args.ItemPath);
                if (fileSystemInfo.Name == folderToAbort)
                {
                    args.IsForceAbortNeeded = true;
                    args.IsForceSkipNeeded = !includeStopDirectory;
                }
            };

            var filtered = visitor.Visit(RootDirectory).ToList();

            var expectedFiltered = new List<string>
            {
                @"C:\Parent",
                @"C:\Parent\Child1"
            };

            if (includeStopDirectory)
            {
                expectedFiltered.Add(@"C:\Parent\Child2");
            }

            CollectionAssert.AreEqual(expectedFiltered, filtered);
        }
        
        [Test]
        public void AbortOnFileFiltered([Values(true, false)] bool includeStopFile)
        {
            const string extensionToAbort = ".pdf";
            
            var visitor = new FileSystemVisitor(FileSystem);
            visitor.FileFilteredEvent += (object sender, FilterEventArgs args) =>
            {
                var fileSystemInfo = FileSystem.FileInfo.FromFileName(args.ItemPath);
                if (fileSystemInfo.Extension == extensionToAbort)
                {
                    args.IsForceAbortNeeded = true;
                    args.IsForceSkipNeeded = !includeStopFile;
                }
            };

            var filtered = visitor.Visit(RootDirectory).ToList();

            var expectedFiltered = new List<string>
            {
                @"C:\Parent",
                @"C:\Parent\Child1",
                @"C:\Parent\Child2",
                @"C:\Parent\Child3",
                @"C:\Parent\Child1\File1.txt"
            };

            if (includeStopFile)
            {
                expectedFiltered.Add(@"C:\Parent\Child2\File2.pdf");
            }

            CollectionAssert.AreEqual(expectedFiltered, filtered);
        }
        
        [Test]
        public void AbortOnDirectoryFiltered([Values(true, false)] bool includeStopDirectory)
        {
            const string folderToAbort = "Child3";
            
            var visitor = new FileSystemVisitor(FileSystem);
            visitor.DirectoryFilteredEvent += (object sender, FilterEventArgs args) =>
            {
                var fileSystemInfo = FileSystem.DirectoryInfo.FromDirectoryName(args.ItemPath);
                if (fileSystemInfo.Name == folderToAbort)
                {
                    args.IsForceAbortNeeded = true;
                    args.IsForceSkipNeeded = !includeStopDirectory;
                }
            };

            var filtered = visitor.Visit(RootDirectory).ToList();

            var expectedFiltered = new List<string>
            {
                @"C:\Parent",
                @"C:\Parent\Child1",
                @"C:\Parent\Child2"
            };

            if (includeStopDirectory)
            {
                expectedFiltered.Add(@"C:\Parent\Child3");
            }

            CollectionAssert.AreEqual(expectedFiltered, filtered);
        }
    }
}