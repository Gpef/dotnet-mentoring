﻿using System.Collections.Generic;
using System.IO.Abstractions;
using System.Linq;
using Module2;
using Module2.CustomEventArgs;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public class CommonTests : TestBase
    {
        [Test]
        public void FilterAllWithoutFilteringMethod()
        {
            var visitor = new FileSystemVisitor(FileSystem);
            var filtered = visitor.Visit(RootDirectory).ToList();

            var expectedFiltered = new List<string>
            {
                @"C:\Parent",
                @"C:\Parent\Child1",
                @"C:\Parent\Child2",
                @"C:\Parent\Child3",
                @"C:\Parent\Child1\File1.txt",
                @"C:\Parent\Child2\File2.pdf",
                @"C:\Parent\Child2\File3.docx",
                @"C:\Parent\Child3\File4.pdf",
            };

            CollectionAssert.AreEqual(expectedFiltered, filtered);
        }
    }
}