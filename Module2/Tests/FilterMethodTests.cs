using System.Collections.Generic;
using System.Linq;
using Module2;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public class FilterMethodTests : TestBase
    {
        [Test]
        public void IncludeOnlyPdfWithFilteringMethod()
        {
            var visitor = new FileSystemVisitor(FileSystem, FilterPdfOnly);
            var filtered = visitor.Visit(RootDirectory).ToList();

            var expectedFiltered = new List<string>
            {
                @"C:\Parent\Child2\File2.pdf",
                @"C:\Parent\Child3\File4.pdf"
            };

            CollectionAssert.AreEqual(expectedFiltered, filtered);
        }
    }
}