using System.Collections.Generic;
using System.IO.Abstractions;
using System.Linq;
using Module2;
using Module2.CustomEventArgs;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public class UseCasesTests : TestBase
    {
        [Test]
        public void IncludeOnlyFilesExceptBelowSLengthInEvent()
        {
            var visitor = new FileSystemVisitor(FileSystem, FilterFilesOnly);
            visitor.FileFoundEvent += (object sender, FoundEventArgs args) =>
            {
                var fileInfo = FileSystem.FileInfo.FromFileName(args.ItemPath);
                if (fileInfo.Length < 4)
                {
                    args.IsForceSkipNeeded = true;
                }
            };

            var filtered = visitor.Visit(RootDirectory).ToList();

            var expectedFiltered = new List<string>
            {
                @"C:\Parent\Child1\File1.txt",
                @"C:\Parent\Child2\File3.docx"
            };

            CollectionAssert.AreEqual(expectedFiltered, filtered);
        }
    }
}