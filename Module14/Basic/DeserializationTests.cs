﻿using Basic.Models;
using DeepEqual.Syntax;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace Basic
{
    [TestFixture]
    public class DeserializationTests
    {
        [Test]
        public void DeserializeAndCheckFirst()
        {
            var expectedBook = new Book
            {
                Author = "Löwy, Juval",
                Description = "\n      COM & .NET Component Services provides both traditional COM programmers and new .NET\n      component developers with the information they need to begin developing applications that take full advantage of COM+ services.\n      This book focuses on COM+ services, including support for transactions, queued components, events, concurrency management, and security\n    ",
                Genres = new List<string> { "Computer", "Fiction" },
                Id = "bk101",
                Isbn = "0-596-00103-7",
                PublishDate = new DateTime(2001, 9, 1),
                Publisher = "O'Reilly",
                RegistrationDate = new DateTime(2016, 1, 5),
                Title = "COM and .NET Component Services"
            };
            var serializer = new XmlSerializer(typeof(Catalog));
            Catalog catalog;

            using (var file = new FileStream(Path.Combine(AppContext.BaseDirectory, @"books.xml"), FileMode.Open))
            {
                catalog = (Catalog)serializer.Deserialize(file);
            }

            var actualBook = catalog.Books.First();
            actualBook.ShouldDeepEqual(expectedBook);
        }

        [Test]
        public void SerializeAndCheck()
        {
            var serializedFile = Path.Combine(AppContext.BaseDirectory, @"books-serialized.xml");

            var expectedBook = new Book
            {
                Author = "Löwy, Juval",
                Description = "\n      COM & .NET Component Services provides both traditional COM programmers and new .NET\n      component developers with the information they need to begin developing applications that take full advantage of COM+ services.\n      This book focuses on COM+ services, including support for transactions, queued components, events, concurrency management, and security\n    ",
                Genres = new List<string> { "Computer", "Fiction" },
                Id = "bk101",
                Isbn = "0-596-00103-7",
                PublishDate = new DateTime(2001, 9, 1),
                Publisher = "O'Reilly",
                RegistrationDate = new DateTime(2016, 1, 5),
                Title = "COM and .NET Component Services"
            };
            var serializer = new XmlSerializer(typeof(Catalog));
            var catalog = new Catalog { Books = new List<Book> { expectedBook } };
            using (var file = new FileStream(serializedFile, FileMode.Create))
            {
                serializer.Serialize(file, catalog);
            }

            Catalog actualCatalog;

            using (var file = new FileStream(serializedFile, FileMode.Open))
            {
                actualCatalog = (Catalog)serializer.Deserialize(file);
            }

            var actualBook = actualCatalog.Books.First();
            actualBook.ShouldDeepEqual(expectedBook);
        }
    }
}