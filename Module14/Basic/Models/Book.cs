﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace Basic.Models
{
    [XmlType(TypeName = "book")]
    public class Book
    {
        [XmlAttribute("id")] public string Id { get; set; }
        [XmlElement("isbn")] public string Isbn { get; set; }
        [XmlElement("author")] public string Author { get; set; }
        [XmlElement("title")] public string Title { get; set; }

        [XmlIgnore]
        public IEnumerable<string> Genres
        {
            get => GenreCollection;
            set => GenreCollection = new XmlCollectionWrapper<string>(value);
        }

        [XmlElement("genre")]
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        public XmlCollectionWrapper<string> GenreCollection { get; set; }

        [XmlElement("publisher")] public string Publisher { get; set; }
        [XmlElement("description")] public string Description { get; set; }

        [XmlElement("publish_date", DataType = "date")]
        public DateTime? PublishDate { get; set; }

        [XmlElement("registration_date", DataType = "date")]
        public DateTime? RegistrationDate { get; set; }

        public Book() { }
    }
}