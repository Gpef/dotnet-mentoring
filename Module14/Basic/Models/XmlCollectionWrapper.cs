﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Basic.Models
{
    public class XmlCollectionWrapper<T> : ICollection<T>
    {
        private readonly IEnumerable<T> _enumerable;

        public int Count => AsCollection().Count();

        public bool IsReadOnly => AsCollection().IsReadOnly;

        private ICollection<T> AsCollection()
        {
            if (!(_enumerable is ICollection<T> collection))
            {
                throw new InvalidOperationException("Only used during deserialization");
            }
            return collection;
        }

        public XmlCollectionWrapper()
        {
            _enumerable = new List<T>();
        }

        public XmlCollectionWrapper(IEnumerable<T> enumerable)
        {
            _enumerable = enumerable;
        }

        public void Add(T item) => AsCollection().Add(item);

        public void Clear() => AsCollection().Clear();

        public bool Contains(T item) => AsCollection().Contains(item);

        public void CopyTo(T[] array, int arrayIndex) => AsCollection().CopyTo(array, arrayIndex);

        public bool Remove(T item) => AsCollection().Remove(item);

        public IEnumerator<T> GetEnumerator() => AsCollection().GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => 
            ((IEnumerable)AsCollection()).GetEnumerator();
    }
}