﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Basic.Models
{
    [Serializable]
    [XmlRoot("catalog", Namespace = "http://library.by/catalog")]
    public class Catalog
    {
        [XmlIgnore]
        public IEnumerable<Book> Books
        {
            get => BooksCollection;
            set => BooksCollection = new XmlCollectionWrapper<Book>(value);
        }

        [XmlElement("book")]
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        public XmlCollectionWrapper<Book> BooksCollection { get; set; }

        [XmlAttribute("date", DataType = "date")]
        public DateTime Date { get; set; }

        public Catalog() { }

        public XmlSchema GetSchema() => null;

        public void ReadXml(XmlReader reader)
        {
            var xRoot = new XmlRootAttribute
            {
                ElementName = "catalog",
                Namespace = "http://library.by/catalog",
                IsNullable = false
            };

            reader.MoveToAttribute("date");
            Date = DateTime.Parse(reader.GetAttribute("date"));
            reader.MoveToElement();

            var ds = new XmlSerializer(typeof(List<Book>), xRoot);
            var parsed = (List<Book>)ds.Deserialize(reader);
            Books = parsed;
        }

        public void WriteXml(XmlWriter writer)
        {
            new XmlSerializer(typeof(Catalog)).Serialize(writer, this);
        }
    }
}