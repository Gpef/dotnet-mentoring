﻿using System;
using System.CodeDom;
using System.Collections.ObjectModel;
using System.Data.Entity.Core.Objects;
using System.Reflection;
using System.Runtime.Serialization;
using Task.DB;

namespace Custom
{
    public class OrderContractSurrogate : IDataContractSurrogate
    {
        #region Not Implemented

        public object GetCustomDataToExport(MemberInfo memberInfo, Type dataContractType) => throw new NotImplementedException();

        public object GetCustomDataToExport(Type clrType, Type dataContractType) => throw new NotImplementedException();

        public void GetKnownCustomDataTypes(Collection<Type> customDataTypes)
        {
            throw new NotImplementedException();
        }

        public Type GetReferencedTypeOnImport(string typeName, string typeNamespace, object customData) => throw new NotImplementedException();

        public CodeTypeDeclaration ProcessImportedType(CodeTypeDeclaration typeDeclaration, CodeCompileUnit compileUnit) => throw new NotImplementedException();


        #endregion

        private ObjectContext _context;

        public OrderContractSurrogate(ObjectContext context)
        {
            _context = context;
        }

        public Type GetDataContractType(Type type)
        {
            return type;
        }

        public object GetObjectToSerialize(object obj, Type targetType)
        {

            //return Convert.ChangeType(obj, targetType);

            switch (obj)
            {
                case Order order:
                    _context.LoadProperty(order, f => f.Employee);
                    _context.LoadProperty(order, f => f.Shipper);
                    _context.LoadProperty(order, f => f.Order_Details);
                    _context.LoadProperty(order, f => f.Customer);

                    var newOrder = new Order
                    {
                        OrderID = order.OrderID,
                        Shipper = order.Shipper,
                        Order_Details = order.Order_Details,
                        Employee = order.Employee,
                        Customer = order.Customer
                    };

                    return newOrder;
                case Shipper shipper:
                    return new Shipper
                    {
                        ShipperID = shipper.ShipperID
                    };
                case Order_Detail order_Detail:
                    return new Order_Detail
                    {
                        OrderID = order_Detail.OrderID,
                        ProductID = order_Detail.ProductID
                    };
                case Customer customer:
                    return new Customer
                    {
                        CustomerID = customer.CustomerID
                    };
                case Employee employee:
                    return new Employee
                    {
                        EmployeeID = employee.EmployeeID

                    };
                default:
                    return obj;
            }
        }

        public object GetDeserializedObject(object obj, Type targetType)
        {
            return obj;
        }

    }
}
