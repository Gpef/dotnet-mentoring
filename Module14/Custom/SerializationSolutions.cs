﻿using Custom;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Runtime.Serialization;
using Task.DB;
using Task.TestHelpers;

namespace Task
{
    [TestClass]
    public class SerializationSolutions
    {
        private Northwind _dbContext;
        private StreamingContext StreamingContext => new StreamingContext(
            StreamingContextStates.CrossProcess, ((IObjectContextAdapter)_dbContext).ObjectContext);

        [TestInitialize]
        public void Initialize()
        {
            _dbContext = new Northwind();
        }

        [TestMethod]
        public void SerializationCallbacks()
        {
            _dbContext.Configuration.ProxyCreationEnabled = false;

            var tester = new XmlDataContractSerializerTester<IEnumerable<Category>>(new NetDataContractSerializer(StreamingContext), true);
            var categories = _dbContext.Categories.ToList();

            tester.SerializeAndDeserialize(categories);
        }

        [TestMethod]
        public void ISerializable()
        {
            _dbContext.Configuration.ProxyCreationEnabled = false;

            var tester = new XmlDataContractSerializerTester<IEnumerable<Product>>(new NetDataContractSerializer(StreamingContext), true);
            var products = _dbContext.Products.ToList();

            tester.SerializeAndDeserialize(products);
        }


        [TestMethod]
        public void ISerializationSurrogate()
        {
            _dbContext.Configuration.ProxyCreationEnabled = false;

            var surrogateSelector = new SurrogateSelector();
            var sc = StreamingContext;
            surrogateSelector.AddSurrogate(typeof(Order_Detail), sc, new OrderDetailSerializationSurrogate());

            var tester = new XmlDataContractSerializerTester<IEnumerable<Order_Detail>>(
                new NetDataContractSerializer(sc) { SurrogateSelector = surrogateSelector },
                true);
            var orderDetails = _dbContext.Order_Details.ToList();

            tester.SerializeAndDeserialize(orderDetails);
        }

        [TestMethod]
        public void IDataContractSurrogate()
        {
            _dbContext.Configuration.ProxyCreationEnabled = true;
            _dbContext.Configuration.LazyLoadingEnabled = true;

            var orders = _dbContext.Orders.ToList();

            var tester = new XmlDataContractSerializerTester<IEnumerable<Order>>(
                new DataContractSerializer(
                   orders.GetType(),
                    new DataContractSerializerSettings
                    {
                        PreserveObjectReferences = true,
                        DataContractSurrogate = new OrderContractSurrogate(((IObjectContextAdapter)_dbContext).ObjectContext),
                        KnownTypes = ObjectContext.GetKnownProxyTypes()
                    }),
                true);

            tester.SerializeAndDeserialize(orders);
        }
    }
}
