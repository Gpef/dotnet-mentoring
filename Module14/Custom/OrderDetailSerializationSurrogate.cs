﻿using System.Data.Entity.Core.Objects;
using System.Runtime.Serialization;
using Task.DB;

namespace Custom
{
    public class OrderDetailSerializationSurrogate : ISerializationSurrogate
    {
        public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
        {
            var orderDetail = (Order_Detail)obj;

            info.AddValue(nameof(orderDetail.OrderID), orderDetail.OrderID);
            info.AddValue(nameof(orderDetail.ProductID), orderDetail.ProductID);
            info.AddValue(nameof(orderDetail.UnitPrice), orderDetail.UnitPrice);
            info.AddValue(nameof(orderDetail.Quantity), orderDetail.Quantity);
            info.AddValue(nameof(orderDetail.Discount), orderDetail.Discount);


            var objContext = (ObjectContext)context.Context;
            objContext.LoadProperty(orderDetail, f => f.Order);
            objContext.LoadProperty(orderDetail, f => f.Product);

            info.AddValue(nameof(orderDetail.Order), orderDetail.Order);
            info.AddValue(nameof(orderDetail.Product), orderDetail.Product);
        }

        public object SetObjectData(object obj, SerializationInfo info, StreamingContext context,
            ISurrogateSelector selector)
        {
            if (!(obj is Order_Detail orderDetail)) return obj;
            orderDetail.OrderID = info.GetInt32(nameof(orderDetail.OrderID));
            orderDetail.ProductID = info.GetInt32(nameof(orderDetail.ProductID));
            orderDetail.UnitPrice = info.GetDecimal(nameof(orderDetail.UnitPrice));
            orderDetail.Quantity = info.GetInt16(nameof(orderDetail.Quantity));
            orderDetail.Discount = info.GetInt32(nameof(orderDetail.Discount));

            orderDetail.Order = info.GetValue(nameof(orderDetail.Order), typeof(Order)) as Order;
            orderDetail.Product = info.GetValue(nameof(orderDetail.Product), typeof(Product)) as Product;

            return orderDetail;
        }
    }
}
