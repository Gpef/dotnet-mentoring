﻿using BenchmarkDotNet.Attributes;
using StringConverter;
using System;

namespace Benchmark
{
    [ClrJob(baseline: true)]
    [RankColumn]
    public class StringConverterBecnhmark
    {
        [Params(10000, 100000)]
        public int n;
        private string data;
        private readonly Converter _converter = new Converter();
        private readonly ConverterWithConst _converterWithConst = new ConverterWithConst();

        [GlobalSetup]
        public void Setup()
        {
            data = "2147483640";
        }

        [Benchmark]
        public int WithCharIsDigit() => _converter.StringToInt(data);

        [Benchmark]
        public int WithIntConst() => _converterWithConst.StringToInt(data);

        [Benchmark]
        public int ConverToInt32() => Convert.ToInt32(data);

        [Benchmark]
        public int IntParse() => int.Parse(data);
    }
}
