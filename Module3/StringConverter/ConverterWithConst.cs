﻿using System;

namespace StringConverter
{
    public class ConverterWithConst
    {
        private const int ZERO_INT_VALUE = 48;
        private const int NINE_INT_VALUE = 57;
        private const int MINUS_CHAR_VALUE = 45;

        /// <summary>
        /// Converts specified string to a number if possible.
        /// </summary>
        /// <param name="input">String to convert to integer.</param>
        /// <returns>Converted to number input string.</returns>
        /// <exception cref="ArgumentNullException">If input string is null.</exception>
        /// <exception cref="ArgumentException">If input string cannot be converted to a number.</exception>
        /// <exception cref="OverflowException">if input string is too large (in positive or negative side) to be stored in int</exception>
        public int StringToInt(string input)
        {
            if (input == null) throw new ArgumentNullException(nameof(input));
            if (input.Length == 0) throw new ArgumentException("Input string is empty", nameof(input));
            if (input.Length > 11) throw new ArgumentException("Input string is too long to be parsed to an integer", nameof(input));

            var negativeCoefficient = 1;
            var startIndex = 0;
            if (input[0] == MINUS_CHAR_VALUE)
            {
                if (input.Length < 2)
                {
                    throw new ArgumentException("Input string cannot be converted to a number", nameof(input));
                }

                negativeCoefficient = -1;
                startIndex++;
            }

            var output = 0;
            for (var i = startIndex; i < input.Length; i++)
            {
                var nextChar = input[i];
                if (nextChar < ZERO_INT_VALUE || nextChar > NINE_INT_VALUE)
                    throw new ArgumentException("Input string cannot be converted to a number", nameof(input));
                output = checked(output * 10 + negativeCoefficient * (nextChar - ZERO_INT_VALUE));
            }

            return output;
        }
    }
}