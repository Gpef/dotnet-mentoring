﻿using System;

namespace Task1
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var inputContent = Console.ReadLine();
            while (!string.IsNullOrEmpty(inputContent) &&
                   !string.IsNullOrWhiteSpace(inputContent))
            {
                var output = inputContent[0];
                Console.WriteLine(output);
                inputContent = Console.ReadLine();
            }
            
            Console.WriteLine("Good bye!");
        }
    }
}
