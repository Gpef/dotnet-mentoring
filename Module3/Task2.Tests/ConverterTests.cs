﻿using System;
using System.ComponentModel;
using System.Threading;
using Moq;
using NUnit.Framework;
using StringConverter;

namespace Task2.Tests
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public class ConverterTests
    {
        private Converter Convert => _convertMockLocal.Value.Object;

        private readonly ThreadLocal<Mock<Converter>> _convertMockLocal =
            new ThreadLocal<Mock<Converter>>(() => new Mock<Converter>());

        [Test]
        public void IncorrectInputTest([Values("", "-", null, "a123", "123a", "12a3")]
            string input)
        {
            Assert.Throws(Is.InstanceOf<ArgumentException>(), () => Convert.StringToInt(input));
        }

        [Test]
        [TestCase("-2147483648", int.MinValue)]
        [TestCase("2147483647", int.MaxValue)]
        [TestCase("0", 0)]
        [TestCase("-0", 0)]
        public void PositiveInputTest(string input, int output)
        {
            Assert.AreEqual(output, Convert.StringToInt(input));
        }

        [Test]
        public void OverflowInputTest([Values("-2147483649", "2147483648")] string input)
        {
            Assert.Throws(Is.InstanceOf<OverflowException>(), () => Convert.StringToInt(input));
        }

    }
}