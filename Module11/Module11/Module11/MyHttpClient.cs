﻿using System;
using System.Net.Http;

namespace Module11
{
    public class MyHttpClient
    {
        public HttpResponseMessage Get()
        {
            var client = new HttpClient();
            return client.GetAsync(new Uri("https://api.ipify.org/")).Result;
        }
    }
}