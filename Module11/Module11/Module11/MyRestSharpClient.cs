﻿using System;
using RestSharp;

namespace Module11
{
    public class MyRestSharpClient
    {
        public string GetIp()
        {
            var client = new RestClient(new Uri("https://api.ipify.org/"));
            return client.Get(new RestRequest()).Content;
        }
    }
}