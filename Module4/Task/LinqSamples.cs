﻿// Copyright © Microsoft Corporation.  All Rights Reserved.
// This code released under the terms of the 
// Microsoft Public License (MS-PL, http://opensource.org/licenses/ms-pl.html.)
//
//Copyright (C) Microsoft Corporation.  All rights reserved.

using SampleSupport;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using Task.Data;

// Version Mad01

namespace SampleQueries
{
    [Title("LINQ Module")]
    [Prefix("Linq")]
    public class LinqSamples : SampleHarness
    {
        private DataSource dataSource = new DataSource();

        [Category("Restriction Operators")]
        [Title("Where - Task 1")]
        [Description("This sample uses the where clause to find all elements of an array with a value less than 5.")]
        public void Linq1()
        {
            int[] numbers = { 5, 4, 1, 3, 9, 8, 6, 7, 2, 0 };

            var lowNums =
                from num in numbers
                where num < 5
                select num;

            Console.WriteLine("Numbers < 5:");
            foreach (var x in lowNums)
            {
                Console.WriteLine(x);
            }
        }

        [Category("Restriction Operators")]
        [Title("Where - Task 2")]
        [Description("This sample return return all presented in market products")]
        public void Linq2()
        {
            var products =
                from p in dataSource.Products
                where p.UnitsInStock > 0
                select p;

            foreach (var p in products)
            {
                ObjectDumper.Write(p);
            }
        }

        [Category("Restriction Operators")]
        [Title("Where - Task 3")]
        [Description("This sample returns all customers with orders sum more than X")]
        public void Linq3()
        {
            var customers = dataSource.Customers
                .Where(c => c.Orders.Sum(o => o.Total) > 10);

            foreach (var customer in customers)
            {
                ObjectDumper.Write(customer);
            }
        }

        [Category("Order Operators")]
        [Title("Order - Task 4")]
        [Description("This sample returns all customers with their first order date")]
        public void Linq4()
        {
            var customersAndDates = dataSource.Customers
                .Select(c => new
                {
                    Customer = c,
                    FirstOrderDate = c.Orders.Length > 0
                        ? new DateTime?(c.Orders.OrderBy(o => o.OrderDate).First().OrderDate)
                        : null
                });

            foreach (var customerAndDate in customersAndDates)
            {
                ObjectDumper.Write($"{customerAndDate.Customer.CustomerID} {customerAndDate.FirstOrderDate}");
            }
        }

        [Category("Order Operators")]
        [Title("Order - Task 5")]
        [Description(
            "This sample returns all customers ordered by their first order year, month, spend money and customer name")]
        public void Linq5()
        {
            var customersAndDates = dataSource.Customers
                .Where(c => c.Orders.Length > 0)
                .Select(c => new
                {
                    Customer = c,
                    FirstOrderDate = c.Orders.OrderBy(o => o.OrderDate).First().OrderDate
                })
                .OrderBy(x => x.FirstOrderDate.Year)
                .ThenBy(x => x.FirstOrderDate.Month)
                .ThenBy(x => x.Customer.Orders.Select(o => o.Total).Sum())
                .ThenBy(x => x.Customer.CompanyName)
                .ToList();

            foreach (var customerAndDate in customersAndDates)
            {
                ObjectDumper.Write($"{customerAndDate.Customer.CustomerID} " +
                                   $"{customerAndDate.FirstOrderDate.Year} " +
                                   $"{customerAndDate.FirstOrderDate.Month} " +
                                   $"{customerAndDate.Customer.Orders.Select(o => o.Total).Sum()} " +
                                   $"{customerAndDate.Customer.CompanyName}");
            }
        }

        [Category("Restriction Operators")]
        [Title("Where - Task 6")]
        [Description("This sample returns all customers with non-digit postal code or" +
                     "or region is not set or there is no operator code in phone number")]
        public void Linq6()
        {
            var regex = new Regex(@"(\(.*?\))");

            var customers = dataSource.Customers
                .Where(c => string.IsNullOrEmpty(c.Region) ||
                            !int.TryParse(c.PostalCode, out var n) ||
                            !regex.IsMatch(c.Phone));

            foreach (var customer in customers)
            {
                ObjectDumper.Write(customer);
            }
        }

        [Category("Grouping Operators")]
        [Title("Group By - Task 7")]
        [Description("This sample returns products grouped by Category, than nested group by units in stock " +
                     "and finally sorted by unit price")]
        public void Linq7()
        {
            var productsGrouped = dataSource
                .Products
                .GroupBy(p => p.Category, (category, itemsInCategory) => new
                {
                    Category = category,
                    Items = itemsInCategory
                            .GroupBy(item => item.UnitsInStock, (units, itemsWithUnits) => new
                            {
                                UnitsInStock = units,
                                Items = itemsWithUnits
                                    .OrderBy(i => i.UnitPrice)
                            })
                }
                );

            foreach (var group in productsGrouped)
            {
                var category = group.Category;

                foreach (var productGroup in group.Items)
                {
                    var units = productGroup.UnitsInStock;

                    foreach (var product in productGroup.Items)
                    {
                        Console.WriteLine($@"{category} {units} {product}");
                        ObjectDumper.Write(product);
                    }
                }
            }
        }

        public enum PriceRange
        {
            Low = 0,
            Medium = 10,
            High = 20,
            Luxury = 30
        }

        [Category("Grouping Operators")]
        [Title("Range Group By - Task 8")]
        [Description("This sample returns products grouped by price categories")]
        public void Linq8()
        {
            var priceRanges = new[] { PriceRange.Low, PriceRange.Medium, PriceRange.High, PriceRange.Luxury };

            var productsGrouped = dataSource.Products
                .GroupBy(p => priceRanges.LastOrDefault(r => (int)r < p.UnitPrice));

            foreach (var group in productsGrouped)
            {
                var price = group.Key;

                foreach (var product in group)
                {
                    Console.WriteLine($@"{price} {product}");
                    ObjectDumper.Write(product);
                }
            }
        }

        [Category("Statistics Operators")]
        [Title("Average - Task 9")]
        [Description("This sample returns groups by city average order price and orders count")]
        public void Linq9()
        {
            var average = dataSource.Customers
                .GroupBy(c => c.City, (city, customers) =>
                {
                    var customersList = customers.ToList();
                    return new
                    {
                        City = city,
                        AverageSum = customersList.SelectMany(c => c.Orders.Select(o => o.Total)).Average(),
                        AverageCount = customersList.Average(c => c.Orders.Length)
                    };
                });
            foreach (var grouping in average)
            {
                ObjectDumper.Write(grouping);
            }
        }

        [Category("Statistics Operators")]
        [Title("GroupBy - Task 10")]
        [Description("This sample shows each customer activity in 1) month in each year, 2) months, 3) years")]
        public void Linq10()
        {
            var orders = dataSource.Customers.Where(c => c.Orders.Length > 0).SelectMany(c => c.Orders);
            var customers = dataSource.Customers.Where(c => c.Orders.Length > 0);

            var customersOrdersByMonth =
                customers
                    .OrderBy(c => c.CustomerID)
                    .Select(c => new
                    {
                        Id = c.CustomerID,

                        YearAndMonthByYearGroups = c.Orders
                            .GroupBy(order => order.OrderDate.Year)
                            .OrderBy(group => group.Key)
                            .Select(yearGroup => new
                            {
                                Year = yearGroup.Key,
                                YearOrdersCount = yearGroup.Count(),
                                MonthGroups = yearGroup
                                    .GroupBy(order => order.OrderDate.Month)
                                    .OrderBy(group => group.Key)
                                    .Select(monthGroup => new
                                    {
                                        Month = monthGroup.Key,
                                        OrdersCount = monthGroup.Count()
                                    })
                            }),

                        MonthWithoutYearGroups = c.Orders
                            .GroupBy(o => o.OrderDate.Month)
                            .OrderBy(group => group.Key)
                            .Select(monthGroup => new
                            {
                                Month = monthGroup.Key,
                                OrdersCount = monthGroup.Count()
                            })
                    });

            Console.WriteLine("Year and month groups:");
            foreach (var customersGroup in customersOrdersByMonth)
            {
                var customerId = customersGroup.Id;
                foreach (var yearGroup in customersGroup.YearAndMonthByYearGroups)
                {
                    var year = yearGroup.Year;
                    foreach (var monthGroup in yearGroup.MonthGroups)
                    {
                        var month = monthGroup.Month;
                        var count = monthGroup.OrdersCount;
                        ObjectDumper.Write($@"{customerId} Y:{year} M:{month} Count: {count}");
                    }
                }
            }

            Console.WriteLine("Year groups:");
            foreach (var customersGroup in customersOrdersByMonth)
            {
                var customerId = customersGroup.Id;
                foreach (var yearGroup in customersGroup.YearAndMonthByYearGroups)
                {
                    var year = yearGroup.Year;
                    var count = yearGroup.YearOrdersCount;
                    ObjectDumper.Write($@"{customerId} Y:{year} Count: {count}");
                }
            }

            Console.WriteLine("Month groups:");
            foreach (var customersGroup in customersOrdersByMonth)
            {
                var customerId = customersGroup.Id;
                foreach (var monthGroup in customersGroup.MonthWithoutYearGroups)
                {
                    var month = monthGroup.Month;
                    var count = monthGroup.OrdersCount;
                    ObjectDumper.Write($@"{customerId} M:{month} Count: {count}");
                }
            }
        }
    }
}