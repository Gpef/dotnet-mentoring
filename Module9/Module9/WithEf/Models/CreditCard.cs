﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Module9.WithEf.Models
{
    public class CreditCard
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public int EmployeeID { get; set; }
        [Required]
        public string Number { get; set; }
        public DateTime? Expires { get; set; }
        public string CardHolder { get; set; }

        public virtual Employee Employee { get; set; }
    }
}