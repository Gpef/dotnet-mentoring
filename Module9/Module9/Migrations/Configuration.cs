using Module9.WithEf.Models;

namespace Module9.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<NorthwindDbContextEf>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(NorthwindDbContextEf context)
        {
            context.Regions.AddOrUpdate(
                new Region { RegionDescription = "Eastern", RegionID = 1 },
                new Region { RegionDescription = "Western", RegionID = 2 },
                new Region { RegionDescription = "Northern", RegionID = 3 },
                new Region { RegionDescription = "Southern", RegionID = 4 }
                );

            context.Categories.AddOrUpdate(
                new Category { CategoryName = "Beverages", Description = "Soft drinks, coffees, teas, beers, and ales" },
                new Category { CategoryName = "Condiments", Description = "Sweet and savory sauces, relishes, spreads, and seasonings" },
                new Category { CategoryName = "Confections", Description = "Desserts, candies, and sweet breads" },
                new Category { CategoryName = "Dairy Products", Description = "Cheeses" },
                new Category { CategoryName = "Grains/Cereals", Description = "Breads, crackers, pasta, and cereal" },
                new Category { CategoryName = "Meat/Poultry", Description = "Prepared meats" },
                new Category { CategoryName = "Produce", Description = "Dried fruit and bean curd" },
                new Category { CategoryName = "Seafood", Description = "Seaweed and fish" }
                );

            context.Territories.AddOrUpdate(
                new Territory { TerritoryID = "01581", RegionID = 1, TerritoryDescription = "Westboro" },
                new Territory { TerritoryID = "01730", RegionID = 1, TerritoryDescription = "Bedford" },
                new Territory { TerritoryID = "01833", RegionID = 1, TerritoryDescription = "Georgetow" },
                new Territory { TerritoryID = "02116", RegionID = 1, TerritoryDescription = "Boston" },
                new Territory { TerritoryID = "02139", RegionID = 1, TerritoryDescription = "Cambridge" },
                new Territory { TerritoryID = "02184", RegionID = 1, TerritoryDescription = "Braintree" },
                new Territory { TerritoryID = "02903", RegionID = 1, TerritoryDescription = "Providence" },
                new Territory { TerritoryID = "03049", RegionID = 3, TerritoryDescription = "Hollis" }
                );
        }
    }
}
