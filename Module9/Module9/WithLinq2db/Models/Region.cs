﻿using System.Collections.Generic;
using LinqToDB.Mapping;

namespace Module9.WithLinq2db.Models
{
    [Table(Name = "Region")]
    public class Region
    {
        [PrimaryKey, NotNull] public int RegionID { get; set; }
        [Column, NotNull] public string RegionDescription { get; set; }

        [Association(ThisKey = "RegionID", OtherKey = "RegionID", CanBeNull = false)]
        public IEnumerable<Territory> Territories { get; set; }
    }
}