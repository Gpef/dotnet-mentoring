﻿using LinqToDB.Mapping;

namespace Module9.WithLinq2db.Models
{
    [Table(Name = "EmployeeTerritories")]
    public class EmployeeTerritory
    {
        [PrimaryKey(1), NotNull] public int EmployeeID { get; set; }
        [PrimaryKey(1), NotNull] public string TerritoryID { get; set; }

        [Association(ThisKey = "EmployeeID", OtherKey = "EmployeeID", CanBeNull = false)]
        public Employee Employee { get; set; }

        [Association(ThisKey = "TerritoryID", OtherKey = "TerritoryID", CanBeNull = false)]
        public Territory Territory { get; set; }
    }
}