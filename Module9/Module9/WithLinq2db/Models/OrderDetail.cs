﻿using LinqToDB.Mapping;

namespace Module9.WithLinq2db.Models
{
    [Table(Name = "Order Details")]
    public class OrderDetail
    {
        [PrimaryKey, Identity] public int OrderID { get; set; }
        [Column, NotNull] public int ProductID { get; set; }
        [Column, NotNull] public decimal UnitPrice { get; set; }
        [Column, NotNull] public short Quantity { get; set; }
        [Column, NotNull] public double Discount { get; set; }
    }
}