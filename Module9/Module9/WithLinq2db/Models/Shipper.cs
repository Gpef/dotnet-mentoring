﻿using System.Collections.Generic;
using LinqToDB.Mapping;

namespace Module9.WithLinq2db.Models
{
    [Table(Name = "Shippers")]
    public class Shipper
    {
        [PrimaryKey, Identity] public int ShipperID { get; set; }
        [Column, NotNull] public string CompanyName { get; set; }
        [Column, Nullable] public string Phone { get; set; }

        [Association(ThisKey = "ShipperID", OtherKey = "ShipVia", CanBeNull = false)]
        public IEnumerable<Order> Orders { get; set; }
    }
}