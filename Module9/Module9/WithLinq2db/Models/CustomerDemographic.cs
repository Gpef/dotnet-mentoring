﻿using System.Collections.Generic;
using LinqToDB.Mapping;

namespace Module9.WithLinq2db.Models
{
    [Table(Name = "CustomerDemographics")]
    public class CustomerDemographic
    {
        [PrimaryKey, NotNull] public string CustomerTypeID { get; set; }
        [Column, Nullable] public string CustomerDesc { get; set; }

        [Association(ThisKey = "CustomerTypeID", OtherKey = "CustomerTypeID", CanBeNull = false)]
        public IEnumerable<CustomerCustomerDemo> CustomerCustomerDemoes { get; set; }
    }
}