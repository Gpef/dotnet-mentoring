﻿using System.Collections.Generic;
using System.Linq;
using LinqToDB;
using LinqToDB.Tools;
using Module9.WithLinq2db.Models;

namespace Module9.WithLinq2db
{
    public static class UpdateQueries
    {
        public static void AddNewEmployee(Employee toAdd, IList<Territory> territories)
        {
            using (var db = new NorthwindDbContext())
            {
                toAdd.EmployeeID = db.InsertWithInt32Identity(toAdd);
                /*
                foreach (var territory in territories)
                {
                    if (!db.Territory.Any(t => t.TerritoryID == territory.TerritoryID))
                    {
                        db.Insert(territory);
                    }
                }
                */

                db.Territory
                    .Merge()
                    .Using(territories)
                    .OnTargetKey()
                    .InsertWhenNotMatched()
                    .Merge();

                foreach (var territory in territories)
                {
                    db.EmployeeTerritory.Insert(() => new EmployeeTerritory
                    {
                        EmployeeID = toAdd.EmployeeID,
                        TerritoryID = territory.TerritoryID
                    });
                }
            }
        }

        public static void ChangeProductsCategory(int categoryIdFrom, int categoryIdTo)
        {
            using (var db = new NorthwindDbContext())
            {
                db.Product
                    .Where(p => p.CategoryID == categoryIdFrom)
                    .Set(t => t.CategoryID, t => categoryIdTo)
                    .Update();
            }
        }

        public static void AddProductsWithSuppliersAndCategories(IList<Product> products)
        {
            using (var db = new NorthwindDbContext())
            {
                foreach (var product in products)
                {
                    var categoryId = product.Category.CategoryID;
                    if (!db.Category.Any(t => t.CategoryID == product.Category.CategoryID))
                    {
                        categoryId = db.InsertWithInt32Identity(product.Category);
                    }

                    product.Category.CategoryID = categoryId;
                    product.CategoryID = categoryId;

                    var supplierId = product.Supplier.SupplierID;
                    if (!db.Supplier.Any(t => t.SupplierID == product.Supplier.SupplierID))
                    {
                        supplierId = db.InsertWithInt32Identity(product.Supplier);
                    }

                    product.Supplier.SupplierID = supplierId;
                    product.SupplierID = supplierId;
                }
                
                db.Product
                    .Merge()
                    .Using(products)
                    .OnTargetKey()
                    .InsertWhenNotMatched()
                    .Merge();
            }

        }

        public static void ReplaceOrderProductWhereNotShipped(int oldProductId, int newProductId)
        {
            using (var db = new NorthwindDbContext())
            {
                var notShippedId = db.Order.Where(p => p.ShippedDate == null).Select(o => o.OrderID).ToList();
                var ordersAlreadyHaveProductIds = db.OrderDetails
                    .Where(od => od.ProductID == newProductId)
                    .Select(od => od.OrderID)
                    .Distinct();

                db.OrderDetails
                    .Where(od =>
                        od.OrderID.In(notShippedId) &&
                        !od.OrderID.In(ordersAlreadyHaveProductIds) &&
                        od.ProductID == oldProductId)
                    .Set(od => od.ProductID, t => newProductId)
                    .Update();
            }
        }
    }
}