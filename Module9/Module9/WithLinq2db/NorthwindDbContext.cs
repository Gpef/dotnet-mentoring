﻿using LinqToDB;
using LinqToDB.Data;
using Module9.WithLinq2db.Models;

namespace Module9.WithLinq2db
{
    internal class NorthwindDbContext : DataConnection
    {
        public NorthwindDbContext() : base("Northwind")
        {
            LinqToDB.Common.Configuration.Linq.AllowMultipleQuery = true;
        }

        public ITable<Category> Category => GetTable<Category>();
        public ITable<CustomerDemographic> CustomerDemographic => GetTable<CustomerDemographic>();
        public ITable<Customer> Customer => GetTable<Customer>();
        public ITable<Employee> Employee => GetTable<Employee>();
        public ITable<EmployeeTerritory> EmployeeTerritory => GetTable<EmployeeTerritory>();
        public ITable<OrderDetail> OrderDetails => GetTable<OrderDetail>();
        public ITable<Order> Order => GetTable<Order>();
        public ITable<Product> Product => GetTable<Product>();
        public ITable<Region> Region => GetTable<Region>();
        public ITable<Shipper> Shipper => GetTable<Shipper>();
        public ITable<Supplier> Supplier => GetTable<Supplier>();
        public ITable<Territory> Territory => GetTable<Territory>();
    }
}