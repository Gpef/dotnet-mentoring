﻿namespace Module9.WithLinq2db.Groupings
{
    public class RegionAndCountGroup
    {
        public string Region { get; set; }
        public int EmployeesCount { get; set; }
    }
}