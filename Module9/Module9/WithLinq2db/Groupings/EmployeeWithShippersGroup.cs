﻿using System.Collections.Generic;
using Module9.WithLinq2db.Models;

namespace Module9.WithLinq2db.Groupings
{
    public class EmployeeWithShippersGroup
    {
        public Employee Employee { get; set; }
        public IList<Shipper> Shippers { get; set; }
    }
}
