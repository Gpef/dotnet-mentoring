﻿using System.Collections.Generic;
using System.Linq;
using LinqToDB;
using Module9.WithLinq2db.Groupings;
using Module9.WithLinq2db.Models;

namespace Module9.WithLinq2db
{
    public static class SelectQueries
    {
        public static IList<Product> GetAllProducts()
        {
            using (var db = new NorthwindDbContext())
            {
                return db.Product
                    .LoadWith(r => r.Supplier)
                    .LoadWith(r => r.Category)
                    .LoadWith(r => r.OrderDetails)
                    .ToList();
            }
        }

        public static IList<Employee> GetAllEmployees()
        {
            using (var db = new NorthwindDbContext())
            {
                return db.Employee.ToList();
            }
        }

        public static IList<RegionAndCountGroup> GetEmployeesCountByRegion()
        {
            using (var db = new NorthwindDbContext())
            {
                var groups = db.Employee
                    .GroupBy(e => e.Region)
                    .Select(g => new RegionAndCountGroup
                    {
                        Region = g.Key,
                        EmployeesCount = g.Count()
                    })
                    .ToList();

                return groups;
            }
        }

        public static IList<EmployeeWithShippersGroup> GetEmployeeWithShippers()
        {
            using (var db = new NorthwindDbContext())
            {
                var groups = db.Employee
                    .LoadWith(r => r.Orders.First().Shipper)
                    .ToList()
                    .Select(e => new EmployeeWithShippersGroup
                    {
                        Employee = e,
                        Shippers = e.Orders.Select(o => o.Shipper).Distinct().ToList()
                    })
                    .ToList();

                return groups;
            }
        }

        public static Employee GetEmployee(int id)
        {
            using (var db = new NorthwindDbContext())
            {
                return db.Employee
                    .LoadWith(r => r.EmployeeTerritories.First().Territory)
                    .FirstOrDefault(e => e.EmployeeID == id);
            }
        }

        public static IList<Product> GetProductsByCategory(int categoryID)
        {
            using (var db = new NorthwindDbContext())
            {
                return db.Product.Where(p => p.CategoryID == categoryID).ToList();
            }
        }

        public static IList<Order> GetNotShippedOrders()
        {
            using (var db = new NorthwindDbContext())
            {
                return db.Order
                    .LoadWith(o => o.OrderDetails)
                    .Where(o => o.ShippedDate == null)
                    .ToList();
            }
        }
    }
}
