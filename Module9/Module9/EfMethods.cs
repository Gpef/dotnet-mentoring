﻿using System.Data.Entity;
using System.Linq;
using NUnit.Framework;
using NorthwindDbContextEf = Module9.WithEf.Models.NorthwindDbContextEf;

namespace Module9
{
    [TestFixture]
    public class EfMethods
    {
        private class ProductWithCustomers
        {
            public string CustomerName { get; set; }
            public string ProductName { get; set; }
            public string CategoryName { get; set; }
        }

        [OneTimeSetUp]
        public void InitDb()
        {
           Database.SetInitializer(new CreateDatabaseIfNotExists<NorthwindDbContextEf>());
        }

        [Test]
        public void SelectProductByCategory()
        {
            using (var db = new NorthwindDbContextEf())
            {
                var category = db.Categories.First(c => c.CategoryID == 2);
                var productIds = db.Products.Where(p => p.CategoryID == category.CategoryID).Select(p => p.ProductID);

                var productWithCustomers = db.Order_Details
                    .Where(od => productIds.Contains(od.ProductID))
                    .Select(p => new ProductWithCustomers
                    {
                        ProductName = p.Product.ProductName,
                        CategoryName = p.Product.Category.CategoryName,
                        CustomerName = p.Order.Customer.CompanyName
                    })
                    .ToList();

                Assert.IsNotEmpty(productWithCustomers);
                var product = productWithCustomers.First();
                Assert.IsNotNull(product.ProductName);
                Assert.IsNotNull(product.CustomerName);
                Assert.IsNotNull(product.CategoryName);
            }
        }
    }
}
