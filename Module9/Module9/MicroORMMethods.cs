﻿using System.Collections.Generic;
using System.Linq;
using Module9.WithLinq2db;
using Module9.WithLinq2db.Models;
using NUnit.Framework;

namespace Module9
{
    [TestFixture]
    public class MicroOrmMethods
    {
        [Test]
        public void SelectAllProduct()
        {
            var product = SelectQueries.GetAllProducts();
            Assert.Multiple(() =>
            {
                Assert.IsNotEmpty(product);
                Assert.True(product.Any(p => p.Category != null));
                Assert.True(product.Any(p => p.Supplier != null));
                Assert.True(product.Any(p => p.OrderDetails != null));
            });
        }

        [Test]
        public void SelectEmployeesAndRegions()
        {
            var employees = SelectQueries.GetAllEmployees();
            Assert.Multiple(() =>
            {
                Assert.IsTrue(employees.Any(e => e.Region != null));
                Assert.IsNotEmpty(employees);
            });
        }

        [Test]
        public void GetRegionStatistics()
        {
            var groups = SelectQueries.GetEmployeesCountByRegion();
            Assert.Multiple(() =>
            {
                Assert.IsTrue(groups.Any(e => e.Region != null));
                Assert.IsTrue(groups.Any(e => e.EmployeesCount > 0));
                Assert.IsNotEmpty(groups);
            });
        }

        [Test]
        public void GetEmployeesWithShippers()
        {
            var groups = SelectQueries.GetEmployeeWithShippers();
            Assert.Multiple(() =>
            {
                Assert.IsTrue(groups.Any(e => e.Employee != null));
                Assert.IsTrue(groups.Any(e => e.Shippers.Count > 0));
                Assert.IsNotEmpty(groups);
            });
        }

        [Test]
        public void InsertEmployeeWithTerritories()
        {
            var employee = new Employee { LastName = "LName", FirstName = "FName" };
            var territory1 = new Territory { TerritoryID = "98109", TerritoryDescription = "Desc1", RegionID = 1 };
            var territory2 = new Territory { TerritoryID = "98110", TerritoryDescription = "Desc2", RegionID = 1 };

            var territories = new List<Territory> { territory1, territory2 };
            UpdateQueries.AddNewEmployee(employee, territories);

            var addedEmployee = SelectQueries.GetEmployee(employee.EmployeeID);
            var addedEmployeeTerritories = addedEmployee.EmployeeTerritories.Select(t => t.Territory.TerritoryID);
            Assert.Multiple(() =>
            {
                Assert.AreEqual(employee.FirstName, addedEmployee.FirstName);
                CollectionAssert.AreEquivalent(new List<string> { territory1.TerritoryID, territory2.TerritoryID }, addedEmployeeTerritories);
            });
        }

        [Test]
        public void UpdateProductCategory()
        {
            int beforeId = 1;
            int afterId = 2;
            var productIds = SelectQueries.GetProductsByCategory(beforeId).Select(p => p.ProductID);
            UpdateQueries.ChangeProductsCategory(beforeId, afterId);
            var productsAfterUpdateIds = SelectQueries.GetProductsByCategory(afterId).Select(p => p.ProductID);
            CollectionAssert.IsSubsetOf(productIds, productsAfterUpdateIds);
        }

        [Test]
        public void ReplaceProductWhereNotShippedOrder()
        {
            var oldId = 60;
            var newId = 3;
            UpdateQueries.ReplaceOrderProductWhereNotShipped(oldId, newId);
            var notShippedAfterUpdate = SelectQueries.GetNotShippedOrders();
            Assert.IsTrue(notShippedAfterUpdate
                .Any(o => o.OrderDetails
                    .Any(od => od.ProductID == newId)));
        }

        [Test]
        public void BulkProductsAdd()
        {
            var products = SelectQueries.GetAllProducts();

            var categories = products.Select(p => p.Category).ToList();
            var cat1 = categories.First();

            var sup1 = products.Select(p => p.Supplier).First();
            sup1.SupplierID = -1;
            sup1.CompanyName = sup1.CompanyName + " II";
            var product = new Product { ProductName = "Bulk added product", Category = cat1, Supplier = sup1 };

            UpdateQueries.AddProductsWithSuppliersAndCategories(new List<Product> { product });
        }
    }
}
