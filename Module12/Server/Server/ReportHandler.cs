﻿using Server.Data;
using Server.Data.Models;
using Server.Data.ReportGenerator;
using Server.Extensions;
using System;
using System.IO;
using System.Net;
using System.Web;

namespace Server
{
    public class ReportHandler : IHttpHandler
    {
        public bool IsReusable => true;

        public void ProcessRequest(HttpContext context)
        {
            var queryParams = context.Request.QueryString;
            if (queryParams.Count == 0)
            {
                var body = context.Request.ReadFullBody();
                queryParams = HttpUtility.ParseQueryString(body);
            }

            OrdersFilter filter;
            try
            {
                filter = ReportQueryParser.ParseFilter(queryParams);
            }
            catch (Exception e)
            {
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                context.Response.Write($"Error parsing request query. {e.Message}");
                return;
            }

            var reportFormat = context.Request.GetFormatFromAcceptHeader();
            var orders = OrdersQueries.GetOrders(filter);
            var generator = new ReportGeneratorProvider().GetGenerator(reportFormat);
            var outputStream = new MemoryStream();
            generator.Generate(orders, outputStream);

            context.Response.ContentType = generator.GeneratedContentType;
            context.Response.BinaryWrite(outputStream.ToArray());
        }
    }
}