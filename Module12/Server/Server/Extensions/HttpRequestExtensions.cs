﻿using Server.Data.ReportGenerator;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using Server.Data.Models;

namespace Server.Extensions
{
    public static class HttpRequestExtensions
    {
        public static string ReadFullBody(this HttpRequest request)
        {
            string bodyContent;
            using (var receiveStream = request.InputStream)
            {
                using (var readStream = new StreamReader(receiveStream, Encoding.UTF8))
                {
                    bodyContent = readStream.ReadToEnd();
                }
            }
            return bodyContent;
        }

        public static ReportFormat GetFormatFromAcceptHeader(this HttpRequest request)
        {
            var accepts = request.AcceptTypes ?? new string[] { };
            if (accepts.Contains("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) return ReportFormat.Excel;
            if (accepts.Contains("text/xml") || accepts.Contains("application/xml")) return ReportFormat.Xml;

            return ReportFormat.Excel;
        }
    }
}