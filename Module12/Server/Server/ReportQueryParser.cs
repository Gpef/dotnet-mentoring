﻿using System;
using System.Collections.Specialized;
using Server.Data.Models;

namespace Server
{
    public static class ReportQueryParser
    {
        private const string TakeKey = "take";
        private const string SkipKey = "skip";
        private const string CustomerKey = "customer";
        private const string DateFromKey = "dateFrom";
        private const string DateToKey = "dateTo";

        public static OrdersFilter ParseFilter(NameValueCollection queryParameters)
        {
            var dateFrom = queryParameters[DateFromKey];
            var dateTo = queryParameters[DateToKey];

            if (dateFrom != null && dateTo != null)
            {
                throw new ArgumentException("Parameters contain both dateFrom and dateTo keys but only one should be used at the same time.", nameof(queryParameters));
            }

            var take = queryParameters[TakeKey];
            var skip = queryParameters[SkipKey];
            var customer = queryParameters[CustomerKey];

            var filter = new OrdersFilter();
            if (take != null) filter.Take = int.Parse(take);
            if (skip != null) filter.Skip = int.Parse(skip);
            if (customer != null) filter.CustomerId = customer;
            if (dateTo != null) filter.DateTo = DateTime.Parse(dateTo);
            else if (dateFrom != null) filter.DateFrom = DateTime.Parse(dateFrom);

            return filter;
        }
    }
}