﻿namespace Server.Data.Models
{
    public enum ReportFormat
    {
        Xml,
        Excel
    }
}