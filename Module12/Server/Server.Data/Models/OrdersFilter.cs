﻿using System;

namespace Server.Data.Models
{
    public class OrdersFilter
    {
        public int Skip { get; set; } = 0;
        public int Take { get; set; } = 50;
        public string CustomerId { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
    }
}