﻿using LinqToDB.Mapping;
using System;

namespace Server.Data.Models
{
    [Table(Name = "Orders", Schema = "Northwind")]
    public class Order
    {
        [PrimaryKey, Identity] public int OrderID { get; set; }
        [Column, Nullable] public string CustomerID { get; set; }
        [Column, Nullable] public int? EmployeeID { get; set; }
        [Column, Nullable] public DateTime? OrderDate { get; set; }
        [Column, Nullable] public DateTime? RequiredDate { get; set; }
        [Column, Nullable] public DateTime? ShippedDate { get; set; }
        [Column, Nullable] public int ShipVia { get; set; }
        [Column, Nullable] public decimal? Freight { get; set; }
        [Column, Nullable] public string ShipName { get; set; }
        [Column, Nullable] public string ShipAddress { get; set; }
        [Column, Nullable] public string ShipCity { get; set; }
        [Column, Nullable] public string ShipRegion { get; set; }
        [Column, Nullable] public string ShipPostalCode { get; set; }
        [Column, Nullable] public string ShipCountry { get; set; }

        public Order() { }
    }
}