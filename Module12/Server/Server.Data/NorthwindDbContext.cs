﻿using LinqToDB;
using LinqToDB.Data;
using Server.Data.Models;

namespace Server.Data
{
    internal class NorthwindDbContext : DataConnection
    {
        public NorthwindDbContext() : base("Northwind")
        {
            LinqToDB.Common.Configuration.Linq.AllowMultipleQuery = true;
        }

        public ITable<Order> Orders => GetTable<Order>();
    }
}
