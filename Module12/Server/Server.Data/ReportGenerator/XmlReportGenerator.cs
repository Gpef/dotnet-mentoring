﻿using Server.Data.Models;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Server.Data.ReportGenerator
{
    public class XmlReportGenerator : IReportGenerator
    {
        /// <inheritdoc/>
        public void Generate<T>(IEnumerable<T> items, Stream writeStream)
        {
            var settings = new XmlWriterSettings
            {
                Indent = true,
                OmitXmlDeclaration = true
            };

            var namespaces = new XmlSerializerNamespaces();
            namespaces.Add("", "");
            var serializer = new XmlSerializer(typeof(T));
            
            using (var writer = XmlWriter.Create(writeStream, settings))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Report");

                foreach (var item in items)
                {
                    serializer.Serialize(writer, item, namespaces);
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }

        public string GeneratedContentType { get; } = "application/xml";
    }
}