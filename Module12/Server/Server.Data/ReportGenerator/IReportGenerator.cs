﻿using Server.Data.Models;
using System.Collections.Generic;
using System.IO;

namespace Server.Data.ReportGenerator
{
    public interface IReportGenerator
    {
        /// <summary>
        /// Generates report by Orders and stores it in the specified file.
        /// </summary>
        /// <param name="items">Orders collection to put into the report.</param>
        /// <param name="writeStream">Stream to write the report.</param>
        /// <returns>True - report was generated successfully, False - otherwise.</returns>
        void Generate<T>(IEnumerable<T> items, Stream writeStream);

        string GeneratedContentType { get; }
    }
}