﻿using Server.Data.Models;

namespace Server.Data.ReportGenerator
{
    public class ReportGeneratorProvider
    {
        public IReportGenerator GetGenerator(ReportFormat format)
        {
            switch (format)
            {
                case ReportFormat.Xml:
                    return new XmlReportGenerator();
                case ReportFormat.Excel:
                default:
                    return new ExcelReportGenerator();
            }
        }
    }
}