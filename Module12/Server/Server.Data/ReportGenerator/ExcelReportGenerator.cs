﻿using ClosedXML.Excel;
using LinqToDB.Extensions;
using Server.Data.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Server.Data.ReportGenerator
{
    public class ExcelReportGenerator : IReportGenerator
    {
        /// <inheritdoc/>
        public void Generate<T>(IEnumerable<T> items, Stream writeStream)
        {
            var ordersList = items.ToList();
            var properties = ordersList.GetListItemType().GetProperties();

            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Report");

                for (var column = 0; column < properties.Length; column++)
                {
                    worksheet.Cell(1, column + 1).Value = properties[column].Name;
                }

                for (var row = 0; row < ordersList.Count; row++)
                {
                    var order = ordersList[row];

                    for (var column = 0; column < properties.Length; column++)
                    {
                        worksheet.Cell(row + 2, column + 1).Value = properties[column].GetValue(order);
                    }
                }

                workbook.SaveAs(writeStream);
            }
        }

        public string GeneratedContentType { get; } = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    }
}