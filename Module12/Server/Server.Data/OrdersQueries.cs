﻿using Server.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace Server.Data
{
    public static class OrdersQueries
    {
        public static IEnumerable<Order> GetOrders(OrdersFilter filter)
        {
            using (var db = new NorthwindDbContext())
            {
                IEnumerable<Order> orders = db.Orders.OrderBy(o => o.OrderDate);

                if (filter.CustomerId != null)
                {
                    orders = orders.Where(o => o.CustomerID == filter.CustomerId);
                }

                if (filter.DateFrom.HasValue)
                {
                    orders = orders.Where(o =>
                        o.OrderDate == null || o.OrderDate.Value > filter.DateFrom.Value);
                }
                else if (filter.DateTo.HasValue)
                {
                    orders = orders.Where(o =>
                        o.OrderDate == null || o.OrderDate.Value > filter.DateTo.Value);
                }

                orders = orders.Skip(filter.Skip).Take(filter.Take);

                return orders.ToList();
            }
        }
    }
}