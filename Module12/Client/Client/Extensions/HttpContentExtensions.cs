﻿using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace Client.Extensions
{
    public static class HttpContentExtensions
    {
        public static Task ReadAsFileAsync(this HttpContent content, string filePath, bool overwrite = true)
        {
            var pathname = Path.GetFullPath(filePath);
            if (!overwrite && File.Exists(filePath))
            {
                return Task.CompletedTask;
            }

            FileStream fileStream = null;
            try
            {
                fileStream = new FileStream(pathname, FileMode.Create, FileAccess.Write, FileShare.None);
                return content.CopyToAsync(fileStream)
                    .ContinueWith(
                        copyTask => { fileStream.Close(); });
            }
            finally
            {
                fileStream?.Close();
            }
        }
    }
}