﻿using System;

namespace Client.Extensions
{
    public static class UriExtensions
    {
        public static bool IsSameHost(this Uri baseUri, Uri compareWithUri) =>
            baseUri.Host.Equals(compareWithUri.Host);

        public static bool IsSameBase(this Uri baseUri, Uri compareWithUri)
        {
            var leftBase = baseUri.GetLeftPart(UriPartial.Path);
            var rightBase = compareWithUri.GetLeftPart(UriPartial.Path);
            return leftBase == rightBase;
        }
    }
}
