﻿using Client.Options;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Client.Extensions
{
    public static class CollectionExtensions
    {
        /// <summary>
        /// Creates a list of <see cref="Uri"/> with valid urls only merged with <see cref="baseUri"/>
        /// representing absolute path to a resource.
        /// </summary>
        /// <param name="uris">Uris to validate and merge.</param>
        /// <param name="baseUri">Base uri to combine with relative uris.</param>
        /// <returns>List of valid uris merged with baseUri where relative.</returns>
        public static IEnumerable<Uri> MergeValidWithBaseUri(this IEnumerable<string> uris, Uri baseUri)
        {
            var merged = new List<Uri>();
            var uriStrings = uris.ToList();

            foreach (var uriString in uriStrings)
            {
                try
                {
                    if (uriString.Equals("/") || uriString.StartsWith("#"))
                    {
                        merged.Add(new Uri(baseUri, uriString));
                    }
                    else if (uriString.StartsWith("//"))
                    {
                        merged.Add(new Uri($"http:{uriString}"));
                    }
                    else if (uriString.StartsWith("/"))
                    {
                        merged.Add(new Uri(new Uri($"{baseUri.Scheme}://{baseUri.Host}"), uriString.Substring(1)));
                    }
                    else
                    {
                        merged.Add(new Uri(uriString));
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Error during parsing \"{uriString}\" url. {e.Message}");
                }
            }

            return merged;
        }

        public static IEnumerable<Uri> FilterAllowedReferences(this IEnumerable<Uri> uris, Uri rootUri, DownloadOptions options)
        {
            switch (options.Restrictions)
            {
                case RedirectRestrictions.CurrentDomainOnly:
                    return uris.Where(u => u.IsSameHost(rootUri));
                case RedirectRestrictions.CurrentBaseUrlOnly:
                    return uris.Where(u => u.IsSameBase(rootUri));
                case RedirectRestrictions.None:
                default:
                    return uris;
            }
        }

        public static IEnumerable<Uri> FilterAllowedResources(this IEnumerable<Uri> resources, DownloadOptions options)
        {
            return options.ResourceExtensions.Count > 0
                ? resources.Where(u => options.ResourceExtensions.Any(u.ToString().EndsWith))
                : resources;
        }


        public static void Enqueue<T>(this Queue<T> queue, IEnumerable<T> items)
        {
            foreach (var currentItem in items)
            {
                queue.Enqueue(currentItem);
            }
        }
    }
}
