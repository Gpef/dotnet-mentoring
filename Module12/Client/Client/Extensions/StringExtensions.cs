﻿using System;
using System.IO;
using System.Linq;

namespace Client.Extensions
{
    public static class StringExtensions
    {
        public static string ToLegalFilename(this string filename, char replacement = '_')
        {
            if (Path.GetInvalidFileNameChars().Contains(replacement))
            {
                throw new ArgumentException($"Replacement charcater {replacement} is illigel for paths or file names.", nameof(replacement));
            }

            return string.Join(replacement.ToString(), filename.Split(Path.GetInvalidFileNameChars()));
        }

    }
}