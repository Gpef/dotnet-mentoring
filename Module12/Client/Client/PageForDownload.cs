﻿using Client.Options;
using System;

namespace Client
{
    internal struct PageForDownload
    {
        public ParsedHtmlPage Page { get; set; }
        public Uri PageUri { get; set; }
        public DownloadOptions Options { get; set; }
        public string SavePath { get; set; }
    }
}