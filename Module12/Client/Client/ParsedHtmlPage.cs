﻿using Client.Options;
using HtmlAgilityPack;
using System.Collections.Generic;
using System.Linq;
using System.Xml.XPath;

namespace Client
{
    public class ParsedHtmlPage
    {
        public HtmlDocument HtmlDocument { get; private set; }

        internal ParsedHtmlPage(string htmlContent)
        {
            HtmlDocument = new HtmlDocument();
            HtmlDocument.LoadHtml(htmlContent);
            RemoveIframes();
        }

        private void RemoveIframes()
        {
            var xpath = XPathExpression.Compile("//iframe");
            var nodes = HtmlDocument.DocumentNode.SelectNodes(xpath);
            var toRemove = nodes != null ? nodes.ToList() : new List<HtmlNode>();
            foreach (var node in toRemove)
            {
                node.Remove();
            }
        }

        public string GetPageTitle()
        {
            var xpathCompiled = XPathExpression.Compile("//head/title");
            var title = HtmlDocument.DocumentNode
                .SelectNodes(xpathCompiled)?
                .FirstOrDefault()?
                .InnerText;
            return title;
        }

        public IEnumerable<string> Extract(LinkAttribute attribute, bool removeGarbage = true)
        {
            var attributeName = attribute.ToString().ToLower();
            var attributes = removeGarbage 
                ? GetAttributesByXPath(attributeName).Where(a => !a.StartsWith("mailto:"))
                : GetAttributesByXPath(attributeName);
            return attributes;
        }

        private IEnumerable<string> GetAttributesByXPath(string attributeName)
        {
            var xpathCompiled = XPathExpression.Compile($"//body//*[@{attributeName}]");
            var nodes = HtmlDocument.DocumentNode.SelectNodes(xpathCompiled);

            return nodes == null
                ? new List<string>()
                : nodes.Select(n => n.Attributes[attributeName]?.Value).ToList();
        }
    }
}