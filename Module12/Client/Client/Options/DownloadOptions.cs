﻿using System.Collections.Generic;

namespace Client.Options
{
    public struct DownloadOptions
    {
        /// <summary>
        /// Specifies depth of redirects found at the initial page.
        /// </summary>
        public int Depth { get; set; }

        /// <summary>
        /// Specifies restrictions applied to urls found at the site pages.
        /// </summary>
        public RedirectRestrictions Restrictions { get; set; }

        /// <summary>
        /// Specifies file extensions allowed to download.
        /// Empty collection means that there are no restrictions on resources.
        /// </summary>
        public IList<string> ResourceExtensions { get; set; }
    }
}