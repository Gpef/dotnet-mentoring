﻿namespace Client.Options
{
    public enum RedirectRestrictions
    {
        None,
        CurrentDomainOnly,
        CurrentBaseUrlOnly
    }
}