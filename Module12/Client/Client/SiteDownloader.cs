﻿using Client.Extensions;
using Client.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;

namespace Client
{
    public class SiteDownloader
    {
        private const string ResourcesContentFolderName = "files";

        private readonly HttpClient _client;

        public SiteDownloader()
        {
            _client = new HttpClient();
        }

        private ParsedHtmlPage GetHtml(Uri siteUri)
        {
            try
            {
                var content = _client.GetStringAsync(siteUri).Result;
                return new ParsedHtmlPage(content);
            }
            catch (AggregateException e)
            {
                Console.WriteLine($"Error loading page {siteUri}. {e}");
                return null;
            }
        }

        /// <summary>
        /// Downloads page specified by <see cref="siteUri"/> and all referenced pages by depth
        /// and other options specified in <see cref="DownloadOptions"/> options to a folder
        /// specified by <see cref="savePath"/>.
        /// </summary>
        /// <param name="siteUri">Page to begin downloading.</param>
        /// <param name="savePath">Folder to save pages and content.</param>
        /// <param name="options">Downloading options that specify depth of download, resources and references.</param>
        public void Download(Uri siteUri, string savePath, DownloadOptions options)
        {
            if (siteUri == null) throw new NullReferenceException(nameof(savePath));
            if (savePath == null) throw new NullReferenceException(nameof(savePath));
            if (!Directory.Exists(savePath)) throw new ArgumentException($"Directory {savePath} is not exists.", nameof(savePath));

            var pagesToDownLoad = new List<PageForDownload>();
            var hrefsQueue = new Queue<Uri>();
            hrefsQueue.Enqueue(siteUri);

            for (var i = 0; i <= options.Depth; i++)
            {
                var currentDepthHrefs = new List<Uri>();

                while (hrefsQueue.Count > 0)
                {
                    var currentHref = hrefsQueue.Dequeue();
                    var page = GetHtml(currentHref);
                    if (page == null) continue;

                    currentDepthHrefs.AddRange(
                        page.Extract(LinkAttribute.Href)
                            .Distinct()
                            .MergeValidWithBaseUri(currentHref)
                            .FilterAllowedReferences(siteUri, options)
                            .ToList());

                    pagesToDownLoad.Add(new PageForDownload
                    {
                        PageUri = currentHref,
                        Options = options,
                        SavePath = savePath,
                        Page = page
                    });
                }

                currentDepthHrefs = currentDepthHrefs
                    .Where(h => !pagesToDownLoad
                        .Any(p => p.PageUri.Equals(h)))
                    .ToList();
                hrefsQueue.Enqueue(currentDepthHrefs);
            }

            DownloadPages(pagesToDownLoad);

            Console.WriteLine($"{siteUri} download has been finished.");
        }

        private void DownloadPages(IEnumerable<PageForDownload> pages)
        {
            foreach (var pageForDownload in pages)
            {
                DownloadPage(pageForDownload);
            }
        }

        private void DownloadPage(PageForDownload pageForDownload)
        {
            var defaultName = pageForDownload.PageUri.Segments.Last();
            var page = pageForDownload.Page;

            var title = WebUtility.HtmlDecode(page.GetPageTitle() ?? defaultName).ToLegalFilename();
            var fileDownloadPath = Path.Combine(pageForDownload.SavePath, ResourcesContentFolderName);
            Directory.CreateDirectory(fileDownloadPath);

            var editedPageHtml = page.HtmlDocument.DocumentNode.InnerHtml;
            var resources = page.Extract(LinkAttribute.Src)
                .Distinct()
                .MergeValidWithBaseUri(pageForDownload.PageUri)
                .FilterAllowedResources(pageForDownload.Options);

            foreach (var resourceUrl in resources)
            {
                var resourceUrlString = resourceUrl.ToString();

                try
                {
                    var filename = resourceUrl.Segments.Last().ToLegalFilename();
                    var resourceDownloadPath = Path.Combine(fileDownloadPath, filename);
                    new WebClient().DownloadFile(resourceUrl, resourceDownloadPath);

                    var newUrl = TransformUrlForLocalStorage(resourceUrlString, ResourcesContentFolderName);
                    editedPageHtml = editedPageHtml.Replace(resourceUrlString, newUrl);
                }
                catch (WebException e)
                {
                    Console.WriteLine($"Error during downloading of {resourceUrl}. {e.Message}");
                }
            }

            var htmlPagePath = Path.Combine(pageForDownload.SavePath, $"{title}.html");
            if (File.Exists(htmlPagePath))
            {
                File.Delete(htmlPagePath);
            }
            File.AppendAllText(htmlPagePath, editedPageHtml, Encoding.UTF8);
        }

        private static string TransformUrlForLocalStorage(string url, string localPath)
        {
            while (url.StartsWith("/"))
            {
                url = url.Substring(1);
            }

            try
            {
                var uri = new Uri(url);
                url = uri.IsAbsoluteUri
                    ? $"./{localPath}/{uri.Segments.Last()}"
                    : $"./{localPath}/{url}";
            }
            catch (Exception e)
            {
                Console.WriteLine($"Impossible to convert \"{url}\" url to local format. {e.Message}");
            }

            return url;
        }

    }
}
