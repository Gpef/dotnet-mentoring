﻿using Client;
using System;
using System.Collections.Generic;
using Client.Options;

namespace ConsoleAppThatUsesClient
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var client = new SiteDownloader();
            var options = new DownloadOptions
            {
                Depth = 2,
                Restrictions = RedirectRestrictions.CurrentDomainOnly,
                ResourceExtensions = new List<string> {  }
            };
            var site = new Uri("http://godville.net");
            var path = @"E:\Downloads\site\1";
            client.Download(site, path, options);
        }
    }
}