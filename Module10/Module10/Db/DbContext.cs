﻿using Module10.Models;
using MongoDB.Driver;
using System.Configuration;

namespace Module10.Db
{
    public class DbContext
    {
        private readonly string _connectionString = ConfigurationManager.ConnectionStrings["MongoDb"].ConnectionString;
        private const string DbName = "local";

        public MongoClient Client { get; }
        public IMongoDatabase Database { get; }

        public IMongoCollection<Book> Books { get; }

        public DbContext()
        {
            Client = new MongoClient(_connectionString);
            Database = Client.GetDatabase(DbName);
            Books = Database.GetCollection<Book>("books");
        }
    }
}