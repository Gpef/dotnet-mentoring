﻿using Module10.Db;
using Module10.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using NUnit.Framework;
using System.Collections.Generic;

namespace Module10
{
    [TestFixture, Parallelizable(ParallelScope.Fixtures)]
    public class MongoDbMethods
    {
        private static IEnumerable<Book> Books => new List<Book>
        {
            new Book{Name = "Hobbit", Author = "Tolkien", Count = 5, Genre = new List<string>{"fantasy"}, Year = 2014},
            new Book{Name = "Lord of the rings", Author = "Tolkien", Count = 3, Genre = new List<string>{"fantasy"}, Year = 2015},
            new Book{Name = "Dyadya Stiopa", Author = "Mihalkov", Count = 1, Genre = new List<string>{"kids"}, Year = 2001},
            new Book{Name = "Kolobok", Count = 10, Genre = new List<string>{"kids"}, Year = 2000},
            new Book{Name = "Repka", Count = 11, Genre = new List<string>{"kids"}, Year = 2000},
        };

        private DbContext Db { get; set; }

        [SetUp]
        public void DbSetup()
        {
            Db = new DbContext();
        }

        [Test, Order(1)]
        public void InsertBooks()
        {
            Db.Books.InsertMany(Books);
        }

        [Test]
        public void FindBooksWithCountGreater1()
        {
            var filterWithCount = new BsonDocument(nameof(Book.Count), new BsonDocument("$gt", 1));

            // #2 a
            var bookNames = Db.Books.Find(filterWithCount).Project(b => b.Name).ToList();

            // #2 b
            var booksSorted = Db.Books.Find(filterWithCount).SortBy(b => b.Name).ToList();

            // #2 c
            var limitedBooks = Db.Books.Find(filterWithCount).Limit(3).ToList();

            // #2 d
            var booksCount = Db.Books.CountDocuments(filterWithCount);
        }

        [Test]
        public void FindBooksWithMaxMinCount()
        {
            var min = Db.Books.Find(new BsonDocument()).SortBy(b => b.Count).Limit(1).First();
            var max = Db.Books.Find(new BsonDocument()).SortByDescending(b => b.Count).Limit(1).First();
        }

        [Test]
        public void FindAuthors()
        {
            var filter = new BsonDocument(nameof(Book.Author), new BsonDocument("$ne", BsonNull.Value));
            var authors = Db.Books.Distinct<string>(nameof(Book.Author), filter).ToList();
        }

        [Test]
        public void BooksWithoutAuthors()
        {
            var filter = new BsonDocument(nameof(Book.Author), new BsonDocument("$eq", BsonNull.Value));
            var booksWithoutAuthors = Db.Books.Find(filter).ToList();
        }

        [Test]
        public void RiseCountByOne()
        {
            var update = new UpdateDefinitionBuilder<Book>().Inc(b => b.Count, 1);
            var updated = Db.Books.UpdateMany(b => true, update);
            Assert.Greater(updated.ModifiedCount, 0);
        }

        [Test]
        public void AddGenre()
        {
            const string newGenre = "favority";
            var filter = new FilterDefinitionBuilder<Book>().AnyEq(b => b.Genre, "fantasy") &
                         new FilterDefinitionBuilder<Book>().AnyNe(b => b.Genre, newGenre);
            var update = new UpdateDefinitionBuilder<Book>().Push(b => b.Genre, newGenre);

            var updated = Db.Books.UpdateMany(filter, update).ModifiedCount;
        }

        [Test]
        public void DeleteWhereCountLower3()
        {
            var filter = new FilterDefinitionBuilder<Book>().Lt(b => b.Count, 3);
            var deleted = Db.Books.DeleteMany(filter).DeletedCount;
        }

        [Test]
        public void DeleteAll()
        {
            var deleted = Db.Books.DeleteMany(new BsonDocument()).DeletedCount;
        }
    }
}
