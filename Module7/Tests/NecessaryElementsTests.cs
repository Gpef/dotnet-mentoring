using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BigLibrary.Converter;
using BigLibrary.Entities;
using BigLibrary.Exceptions;
using BigLibrary.IO;
using NUnit.Framework;

namespace Tests
{
    [TestFixture, Parallelizable(ParallelScope.Fixtures)]
    public class NecessaryElementsTests
    {
        private readonly string _filePath = Path.Combine(AppContext.BaseDirectory, "LibExample-book.xml");

        private static IEnumerable<AbstractCatalogItem> CatalogForTests => new List<AbstractCatalogItem>
        {
            new Book
            {
                Annotation = "Anno1",
                Authors = new List<Author> {new Author("Oleg"), new Author("Anton")},
                Catalog = "Catalog My",
                Exported = new DateTime(2018, 2, 3),
                Isbn = "123ead3ts",
                PagesCount = 10,
                Published = new PublishInfo("Minsk", "Booking", 1999)
            }
        };

        [SetUp]
        public void Setup()
        {
            if (File.Exists(_filePath))
            {
                File.Delete(_filePath);
            }

            using (var writeStream = File.OpenWrite(_filePath))
            {
                var x = new CatalogXmlWriter();
                x.Write(writeStream, CatalogForTests, new IoOptions());
            }
        }

        [Test]
        public void BookNecessaryElementWithExceptionTest()
        {
            List<AbstractCatalogItem> list;
            using (var readStream = File.OpenRead(_filePath))
            {
                var reader = new CatalogXmlReader();
                Assert.Throws(typeof(CatalogItemException), () =>
                    list = reader.Read(readStream, new IoOptions { ThrowExceptionOnInvalidNode = true }).ToList());
            }
        }

        [Test]
        public void BookNecessaryElementWithoutExceptionTest()
        {
            List<AbstractCatalogItem> list;
            using (var readStream = File.OpenRead(_filePath))
            {
                var reader = new CatalogXmlReader();
                Assert.DoesNotThrow(() =>
                    list = reader.Read(readStream, new IoOptions { ThrowExceptionOnInvalidNode = false }).ToList());
            }
        }
    }
}