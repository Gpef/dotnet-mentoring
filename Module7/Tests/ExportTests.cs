using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BigLibrary.Converter;
using BigLibrary.Entities;
using BigLibrary.IO;
using DeepEqual.Syntax;
using NUnit.Framework;

namespace Tests
{
    [TestFixture, Parallelizable(ParallelScope.Fixtures)]
    public class ExportTests
    {
        private readonly string _filePath = Path.Combine(AppContext.BaseDirectory, "LibExample.xml");

        private static IEnumerable<AbstractCatalogItem> CatalogForTests => new List<AbstractCatalogItem>
        {
            new Book
            {
                Annotation = "Anno1",
                Authors = new List<Author> {new Author("Oleg"), new Author("Anton")},
                Title = "Book title",
                Catalog = "Catalog My",
                Exported = new DateTime(2018, 2, 3),
                Isbn = "123ead3ts",
                PagesCount = 10,
                Published = new PublishInfo("Minsk", "Booking", 1999)
            },
            new Newspaper
            {
                Title = "Newspaper 1",
                Exported = new DateTime(2006, 1, 2),
                Published = new PublishInfo("Moscow", "Rise", 2001),
                Annotation = "Newspaper anno",
                PagesCount = 30,
                Catalog = "Catalog My",
                Issn = "news iisna ae2asd",
                Issue = 2,
                Date = new DateTime(2001, 2, 4)
            },
            new Patent
            {
                Title = "First patent",
                Exported = new DateTime(2000, 5, 5),
                Annotation = "Patent anno",
                PagesCount = 3,
                Catalog = "Catalog My 1",
                Inventor = "Oleg",
                RegistrationNumber = "123514341aEDS2",
                Country = "USA",
                DateCreated = new DateTime(1990, 3, 3),
                DatePublished = new DateTime(1990, 4, 4),
            }
        };

        [SetUp]
        public void Setup()
        {
            if (File.Exists(_filePath))
            {
                File.Delete(_filePath);
            }

            using (var writeStream = File.OpenWrite(_filePath))
            {
                var x = new CatalogXmlWriter();
                x.Write(writeStream, CatalogForTests, new IoOptions());
            }
        }

        [Test]
        public void CatalogExport()
        {
            List<AbstractCatalogItem> list;
            using (var readStream = File.OpenRead(_filePath))
            {
                var reader = new CatalogXmlReader();
                list = reader.Read(readStream, new IoOptions()).ToList();
            }

            list.ShouldDeepEqual(CatalogForTests);
        }
    }
}