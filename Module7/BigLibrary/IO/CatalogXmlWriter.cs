using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using BigLibrary.Converter;
using BigLibrary.Entities;
using BigLibrary.Extensions;

namespace BigLibrary.IO
{
    public class CatalogXmlWriter : ICatalogWriter
    {
        public void Write(Stream streamWriter, IEnumerable<AbstractCatalogItem> catalog, IoOptions ioOptions)
        {
            if (catalog == null) throw new ArgumentNullException(nameof(catalog));
            if (ioOptions == null) throw new ArgumentNullException(nameof(ioOptions));

            var settings = new XmlWriterSettings
            {
                Indent = true,
                OmitXmlDeclaration = true
            };

            using (var writer = XmlWriter.Create(streamWriter, settings))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Catalog");

                foreach (var item in catalog)
                {
                    if (item == null && ioOptions.ThrowExceptionOnInvalidNode)
                    {
                        throw new ArgumentNullException(nameof(catalog), "element in the catalog");
                    }
                    writer.WriteEntity(item);
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }
    }
}