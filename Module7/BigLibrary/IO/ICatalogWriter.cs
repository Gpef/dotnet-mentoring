using System.Collections.Generic;
using System.IO;
using BigLibrary.Converter;
using BigLibrary.Entities;

namespace BigLibrary.IO
{
    public interface ICatalogWriter
    {
        void Write(Stream streamWriter, IEnumerable<AbstractCatalogItem> catalog, IoOptions ioOption);
    }
}