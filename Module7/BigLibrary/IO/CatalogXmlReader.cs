using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using BigLibrary.Converter;
using BigLibrary.Entities;
using BigLibrary.Exceptions;

namespace BigLibrary.IO
{
    public class CatalogXmlReader : ICatalogReader
    {
        public IEnumerable<AbstractCatalogItem> Read(Stream streamReader, IoOptions ioOptions)
        {
            if (streamReader == null) throw new ArgumentNullException(nameof(streamReader));
            if (ioOptions == null) throw new ArgumentNullException(nameof(ioOptions));

            var parser = new XElementParser();
            
            using (var reader = XmlReader.Create(streamReader))
            {
                reader.MoveToContent();
                while (reader.Read())
                {
                    if (reader.NodeType != XmlNodeType.Element ||
                        !reader.Name.Equals(nameof(Book)) &&
                        !reader.Name.Equals(nameof(Newspaper)) &&
                        !reader.Name.Equals(nameof(Patent))) continue;

                    var element = XNode.ReadFrom(reader) as XElement;

                    AbstractCatalogItem parsedElement;
                    try
                    {
                        parsedElement = parser.Parse(element);
                    }
                    catch (CatalogItemException)
                    {
                        if (ioOptions.ThrowExceptionOnInvalidNode) throw;
                        continue;
                    }

                    yield return parsedElement;
                }
            }
        }

        public IEnumerable<T> Read<T>(Stream streamReader, IoOptions readOptions)
            where T : AbstractCatalogItem, new()
        {
            foreach (var item in Read(streamReader, readOptions))
            {
                if (item is T itemOfNeededType)
                {
                    yield return itemOfNeededType;
                }
            }
        }
    }
}