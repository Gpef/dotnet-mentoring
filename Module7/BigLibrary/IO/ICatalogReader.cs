using System.Collections.Generic;
using System.IO;
using BigLibrary.Converter;
using BigLibrary.Entities;

namespace BigLibrary.IO
{
    public interface ICatalogReader
    {
        IEnumerable<AbstractCatalogItem> Read(Stream streamReader, IoOptions readOptions);
        IEnumerable<T> Read<T>(Stream streamReader, IoOptions readOptions) where T : AbstractCatalogItem, new();
    }
}