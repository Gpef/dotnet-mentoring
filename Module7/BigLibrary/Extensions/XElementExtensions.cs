using System.Xml.Linq;
using System.Xml.Serialization;

namespace BigLibrary.Extensions
{
    public static class XElementExtensions
    {
        public static T To<T>(this XElement xmlElement)
        {
            var xml = xmlElement.CreateReader();
            var serializer = new XmlSerializer(typeof(T));
            return (T) serializer.Deserialize(xml);
        }
    }
}