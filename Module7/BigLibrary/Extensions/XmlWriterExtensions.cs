using System;
using System.Xml;
using System.Xml.Serialization;
using BigLibrary.Entities;

namespace BigLibrary.Extensions
{
    public static class XmlWriterExtensions
    {
        public static void WriteEntity<T>(this XmlWriter writer, T entity) where T : AbstractCatalogItem
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));

            var namespaces = new XmlSerializerNamespaces();
            namespaces.Add("", "");
            
            var serializer = new XmlSerializer(entity.GetType());
            serializer.Serialize(writer, entity, namespaces);
        }
    }
}