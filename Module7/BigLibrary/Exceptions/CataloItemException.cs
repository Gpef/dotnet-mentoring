using System;
using System.Xml;
using System.Xml.Linq;

namespace BigLibrary.Exceptions
{
    [Serializable]
    public class CatalogItemException : XmlException
    {
        public XElement InvalidElement { get; private set; }

        public CatalogItemException(string message, XElement element) : base(message)
        {
            InvalidElement = element;
        }

        public CatalogItemException() { }

        public CatalogItemException(string message) : base(message) { }

        public CatalogItemException(string message, Exception innerException) : base(message, innerException) { }
    }
}