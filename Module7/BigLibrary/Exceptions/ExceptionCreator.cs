using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;

namespace BigLibrary.Exceptions
{
    public class ExceptionCreator
    {
        public static CatalogItemException Create(XElement xElement, string message)
        {
            IXmlLineInfo info = xElement;
            var lineNumber = info.LineNumber;
            var linePosition = info.LinePosition;

            return new CatalogItemException(
                $"Error parsing element at Line:Position {lineNumber}:{linePosition}. {message}",
                xElement);
        }

        public static CatalogItemException Create(XElement xElement, IEnumerable<string> expectedTags) => 
            Create(xElement, $"Some of necessary tags are not present: {string.Join(", ", expectedTags)}");
    }
}