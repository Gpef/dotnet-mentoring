namespace BigLibrary.Entities
{
    public class Author
    {
        public string Name { get; set; }

        public Author(string name)
        {
            Name = name;
        }

        public Author()
        {
        }
    }
}