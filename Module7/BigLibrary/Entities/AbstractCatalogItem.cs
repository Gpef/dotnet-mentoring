using System;
using System.Xml.Serialization;

namespace BigLibrary.Entities
{
    public abstract class AbstractCatalogItem
    {
        [XmlAttribute("exported", DataType = "date")]
        public DateTime Exported { get; set; }

        [XmlAttribute("catalog")] public string Catalog { get; set; }

        public string Title { get; set; }

        protected AbstractCatalogItem() { }
    }
}