﻿using System;

namespace BigLibrary.Entities
{
    public class Patent : AbstractCatalogItem
    {
        public string Inventor { get; set; }
        public string Country { get; set; }
        public string RegistrationNumber { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DatePublished { get; set; }
        public int? PagesCount { get; set; }
        public string Annotation { get; set; }

        public Patent() : base()
        {
        }
    }
}