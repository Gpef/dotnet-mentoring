﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace BigLibrary.Entities
{
    public class Book : AbstractCatalogItem
    {
        public List<Author> Authors { get; set; }
        public PublishInfo Published { get; set; }
        public int? PagesCount { get; set; }
        public string Annotation { get; set; }
        public string Isbn { get; set; }

        public Book() : base()
        {
        }
    }
}