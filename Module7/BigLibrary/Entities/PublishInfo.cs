﻿namespace BigLibrary.Entities
{
    public class PublishInfo
    {
        public string PublisherCity { get; set; }
        public string PublisherName { get; set; }
        public int? YearPublished { get; set; }

        public PublishInfo(string city, string name, int year)
        {
            PublisherCity = city;
            PublisherName = name;
            YearPublished = year;
        }

        internal PublishInfo()
        {
        }
    }
}