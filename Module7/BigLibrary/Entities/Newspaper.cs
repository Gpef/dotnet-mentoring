﻿using System;

namespace BigLibrary.Entities
{
    public class Newspaper : AbstractCatalogItem
    {
        public PublishInfo Published { get; set; }
        public int? PagesCount { get; set; }
        public string Annotation { get; set; }
        public int? Issue { get; set; }
        public DateTime? Date { get; set; }
        public string Issn { get; set; }

        public Newspaper() : base()
        {
        }
    }
}