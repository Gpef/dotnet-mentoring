namespace BigLibrary.Converter
{
    public class IoOptions
    {
        /// <summary>
        /// Specifies if exception should be thrown when parsing invalid node.
        /// </summary>
        public bool ThrowExceptionOnInvalidNode { get; set; } = false;
    }
}