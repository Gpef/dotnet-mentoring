using System;
using System.Xml.Linq;
using BigLibrary.Entities;

namespace BigLibrary.Converter
{
    public class XElementParser
    {
        public AbstractCatalogItem Parse(XElement xElement)
        {
            if (xElement == null) throw new ArgumentNullException(nameof(xElement));

            var nodeName = xElement.Name.LocalName;

            switch (nodeName)
            {
                case nameof(Book):
                    return xElement.ToBook();
                case nameof(Newspaper):
                    return xElement.ToNewspaper();
                case nameof(Patent):
                    return xElement.ToPatent();
                default:
                    throw new NotSupportedException($"Element of type {nodeName}");
            }
        }
    }
}