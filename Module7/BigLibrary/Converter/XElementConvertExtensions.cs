using System.Collections.Generic;
using System.Xml.Linq;
using BigLibrary.Entities;
using BigLibrary.Exceptions;
using BigLibrary.Extensions;

namespace BigLibrary.Converter
{
    public static class XElementConvertExtensions
    {
        private static readonly IList<string> ExpectedBookFields = new List<string>
        {
            nameof(Book.Title), nameof(Book.Authors), nameof(Book.Published)
        };

        private static readonly IList<string> ExpectedPatentFields = new List<string>
        {
            nameof(Patent.Title), nameof(Patent.Inventor), nameof(Patent.RegistrationNumber)
        };

        private static readonly IList<string> ExpectedNewspaperFields = new List<string>
        {
            nameof(Newspaper.Title), nameof(Newspaper.Issn), nameof(Newspaper.Issue)
        };

        public static Book ToBook(this XElement xElement)
        {
            var book = xElement.To<Book>();

            if (book.Title == null ||
                book.Authors == null || book.Authors.Count == 0 ||
                book.Published == null)
                throw ExceptionCreator.Create(xElement, ExpectedBookFields);

            return book;
        }

        public static Patent ToPatent(this XElement xElement)
        {
            var patent = xElement.To<Patent>();

            if (patent.Title == null ||
                patent.Inventor == null ||
                patent.RegistrationNumber == null)
                throw ExceptionCreator.Create(xElement, ExpectedPatentFields);

            return patent;
        }

        public static Newspaper ToNewspaper(this XElement xElement)
        {
            var newspaper = xElement.To<Newspaper>();

            if (newspaper.Title == null ||
                newspaper.Issn == null ||
                newspaper.Issue == null)
                throw ExceptionCreator.Create(xElement, ExpectedNewspaperFields);

            return newspaper;
        }
    }
}