using Module6.Container;
using NUnit.Framework;
using System.Reflection;
using Tests.Data;

namespace Tests
{
    public class TestBase
    {
        protected static MyContainer GetContainerWithManuallyAddedDependencies()
        {
            var container = new MyContainer();
            container.AddType(typeof(CustomerBllWithConst));
            container.AddType(typeof(CustomerBllWithProps));
            container.AddType(typeof(Logger));
            container.AddType(typeof(CustomerDal), typeof(ICustomerDal));

            return container;
        }

        protected static MyContainer GetContainerWithAssemblyAdded()
        {
            var container = new MyContainer();
            container.AddAssembly(Assembly.GetExecutingAssembly());

            return container;
        }

        protected static void AssertObjectsResolvingIsRight(
            CustomerBllWithConst bllWithConst,
            CustomerBllWithProps bllWithProps,
            Logger logger,
            ICustomerDal customerDal)
        {
            Assert.Multiple(() =>
            {
                Assert.NotNull(bllWithConst);
                Assert.That(bllWithConst, Is.TypeOf<CustomerBllWithConst>());

                Assert.NotNull(bllWithProps);
                Assert.That(bllWithProps, Is.TypeOf<CustomerBllWithProps>());

                Assert.NotNull(logger);
                Assert.That(logger, Is.TypeOf<Logger>());

                Assert.NotNull(customerDal);
                Assert.That(customerDal, Is.TypeOf<CustomerDal>());
                Assert.That(customerDal, Is.InstanceOf<ICustomerDal>());
            });
        }
    }
}