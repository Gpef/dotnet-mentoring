using Module6.Container.Attributes;

namespace Tests.Data
{
    [Export(typeof(ICustomerDal))]
    [ImportConstructor]
    public class CustomerDal: ICustomerDal
    {
        
    }
}