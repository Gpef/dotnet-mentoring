using Module6.Container.Attributes;

namespace Tests.Data
{
    public class CustomerBllWithProps
    {
        [Import] public ICustomerDal Dal { get; set; } 
        
        [Import] public Logger Logger { get; set; }
    }
}