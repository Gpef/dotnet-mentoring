using Module6.Container.Attributes;

namespace Tests.Data
{
    [ImportConstructor]
    public class CustomerBllWithConst
    {
        public ICustomerDal Dal { get; set; }
        public Logger Logger { get; set; }
        
        public CustomerBllWithConst(ICustomerDal dal, Logger logger)
        {
            Dal = dal;
            Logger = logger;
        }
    }
}