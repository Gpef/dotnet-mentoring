using Module6.Container;
using Module6.Container.Exceptions;
using NUnit.Framework;
using Tests.Data;

namespace Tests
{
    [TestFixture]
    public class MyContainerTests : TestBase
    {
        [Test]
        public void GenericAssemblyAddedTest()
        {
            var container = GetContainerWithAssemblyAdded();

            var bllWithConst = container.CreateInstance<CustomerBllWithConst>();
            var bllWithProps = container.CreateInstance<CustomerBllWithProps>();
            var logger = container.CreateInstance<Logger>();
            var customerDal = container.CreateInstance<ICustomerDal>();

            Assert.Multiple(() =>
            {
                AssertObjectsResolvingIsRight(bllWithConst, bllWithProps, logger, customerDal);
                Assert.NotNull(bllWithProps.Dal);
                Assert.NotNull(bllWithProps.Logger);
                Assert.That(bllWithProps.Dal, Is.TypeOf<CustomerDal>());
                Assert.That(bllWithProps.Dal, Is.InstanceOf<ICustomerDal>());
                Assert.That(bllWithProps.Logger, Is.TypeOf<Logger>());
            });
        }

        [Test]
        public void GenericManuallyAddedTest()
        {
            var container = GetContainerWithAssemblyAdded();

            var bllWithConst = container.CreateInstance<CustomerBllWithConst>();
            var bllWithProps = container.CreateInstance<CustomerBllWithProps>();
            var logger = container.CreateInstance<Logger>();
            var customerDal = container.CreateInstance<ICustomerDal>();

            Assert.Multiple(() =>
            {
                AssertObjectsResolvingIsRight(bllWithConst, bllWithProps, logger, customerDal);
                Assert.NotNull(bllWithConst.Dal);
                Assert.NotNull(bllWithConst.Logger);
                Assert.That(bllWithConst.Dal, Is.TypeOf<CustomerDal>());
                Assert.That(bllWithConst.Dal, Is.InstanceOf<ICustomerDal>());
                Assert.That(bllWithConst.Logger, Is.TypeOf<Logger>());
            });
        }

        [Test]
        public void ManualCastAssemblyAddedTest()
        {
            var container = GetContainerWithAssemblyAdded();

            var bllWithConst = (CustomerBllWithConst)container.CreateInstance(typeof(CustomerBllWithConst));
            var bllWithProps = (CustomerBllWithProps)container.CreateInstance(typeof(CustomerBllWithProps));
            var logger = (Logger)container.CreateInstance(typeof(Logger));
            var customerDal = (ICustomerDal)container.CreateInstance(typeof(ICustomerDal));

            Assert.Multiple(() =>
            {
                AssertObjectsResolvingIsRight(bllWithConst, bllWithProps, logger, customerDal);
                Assert.NotNull(bllWithProps.Dal);
                Assert.NotNull(bllWithProps.Logger);
                Assert.That(bllWithProps.Dal, Is.TypeOf<CustomerDal>());
                Assert.That(bllWithProps.Dal, Is.InstanceOf<ICustomerDal>());
                Assert.That(bllWithProps.Logger, Is.TypeOf<Logger>());
            });
        }

        [Test]
        public void ManualCastManuallyAddedTest()
        {
            var container = GetContainerWithManuallyAddedDependencies();

            var bllWithConst = (CustomerBllWithConst)container.CreateInstance(typeof(CustomerBllWithConst));
            var bllWithProps = (CustomerBllWithProps)container.CreateInstance(typeof(CustomerBllWithProps));
            var logger = (Logger)container.CreateInstance(typeof(Logger));
            var customerDal = (ICustomerDal)container.CreateInstance(typeof(ICustomerDal));

            Assert.Multiple(() =>
            {
                AssertObjectsResolvingIsRight(bllWithConst, bllWithProps, logger, customerDal);
                Assert.NotNull(bllWithConst.Dal);
                Assert.NotNull(bllWithConst.Logger);
                Assert.That(bllWithConst.Dal, Is.TypeOf<CustomerDal>());
                Assert.That(bllWithConst.Dal, Is.InstanceOf<ICustomerDal>());
                Assert.That(bllWithConst.Logger, Is.TypeOf<Logger>());
            });
        }

        [Test]
        public void AddWithoutAttributes()
        {
            var container = new MyContainer();
            Assert.Throws<ConventionException>(() => container.AddType(typeof(string)));
        }

    }
}