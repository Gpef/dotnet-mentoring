using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Module6.Container.Attributes;

namespace Module6.Extensions
{
    public static class AssemblyExtensions
    {
        public static bool HasImportPropertiesAttribute(this Type type) =>
            type.GetProperties().Any(p => p.GetCustomAttribute<ImportAttribute>() != null);

        public static bool HasImportConstructorAttribute(this Type type) =>
            type.GetCustomAttribute<ImportConstructorAttribute>() != null;
        
        public static bool HasExportAttribute(this Type type) => type.GetCustomAttribute<ExportAttribute>() != null;
        
        public static IList<TypeInfo> GetTypesWithConstructorImport(this Assembly assembly) =>
            assembly.DefinedTypes
                .Where(t => t.GetCustomAttribute<ImportConstructorAttribute>() != null)
                .ToList();

        public static IList<TypeInfo> GetTypesWithPropertiesImport(this Assembly assembly) =>
            assembly.DefinedTypes
                .Where(t => t.GetProperties()
                    .Any(p => p.GetCustomAttribute<ImportAttribute>() != null))
                .ToList();

        public static IList<KeyValuePair<Type, Type>> GetTypesWithTypeToExport(this Assembly assembly) =>
            assembly.DefinedTypes
                .Where(t => t.GetCustomAttribute<ExportAttribute>() != null)
                .Select(t =>
                    new KeyValuePair<Type, Type>(
                        t,
                        t.GetCustomAttribute<ExportAttribute>().TypeToResolve))
                .ToList();
    }
}