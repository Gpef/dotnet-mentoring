using System;

namespace Module6.Container.Exceptions
{
    public class ConventionException : Exception
    {
        public ConventionException() { }

        public ConventionException(string message) : base(message) { }

        public ConventionException(string message, Exception innerException) : base(message, innerException) { }
    }
}