using System;

namespace Module6.Container.Exceptions
{
    public class TypeNotRegisteredException : Exception
    {
        public TypeNotRegisteredException(Type type) :
            base($"Convention for type {type.FullName} is not registered")
        { }

        public TypeNotRegisteredException() { }

        public TypeNotRegisteredException(string message) : base(message) { }

        public TypeNotRegisteredException(string message, Exception innerException) : base(message, innerException) { }
    }
}