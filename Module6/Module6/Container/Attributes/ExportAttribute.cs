using System;

namespace Module6.Container.Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class ExportAttribute : Attribute
    {
        public Type TypeToResolve { get; }

        public ExportAttribute(Type typeToResolve)
        {
            TypeToResolve = typeToResolve;
        }

        public ExportAttribute()
        {
        }
    }
}