using System;

namespace Module6.Container.Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class ImportConstructorAttribute : Attribute
    {
        public ImportConstructorAttribute()
        {
        }
    }
}