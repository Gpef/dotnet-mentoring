namespace Module6.Container.Enums
{
    public enum InjectionType
    {
        Constructor,
        PropertySetter
    }
}