﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using Module6.Container.Attributes;
using Module6.Container.Enums;
using Module6.Container.Exceptions;
using Module6.Extensions;

namespace Module6.Container
{
    public class MyContainer : IContainer
    {
        private readonly IList<RegisteredConvention> _conventions = new List<RegisteredConvention>();

        public void AddType<TConcrete, TToResolve>() => AddType(typeof(TConcrete), typeof(TToResolve));
        public void AddType(Type typeToResolve) => AddType(typeToResolve, typeToResolve);

        public void AddType(Type concreteType, Type typeToResolve)
        {
            var hasImportConstructor = concreteType.HasImportConstructorAttribute();
            var hasImportPropertiesAttribute = concreteType.HasImportPropertiesAttribute();

            if (hasImportConstructor && hasImportPropertiesAttribute)
            {
                throw new ConventionException(
                    $"Type {concreteType} has both {nameof(ImportAttribute)} and {nameof(ImportConstructorAttribute)}. Only one injection type is supported at the same time");
            }

            if (!hasImportConstructor && !hasImportPropertiesAttribute)
            {
                throw new ConventionException(
                    $"Type {concreteType} has no injection attributes and cannot be resolved.");
            }

            var injectionType = hasImportConstructor ? InjectionType.Constructor : InjectionType.PropertySetter;

            AddType(concreteType, typeToResolve, injectionType);
        }

        private void AddType(Type concreteType, Type typeToResolve, InjectionType injectionType)
        {
            var convention = _conventions.FirstOrDefault(c => c.TypeToResolve == typeToResolve);
            if (null == convention)
            {
                _conventions.Add(new RegisteredConvention(concreteType, typeToResolve, injectionType));
            }
            else
            {
                convention.ConcreteType = concreteType;
            }
        }

        public void AddAssembly(Assembly assembly)
        {
            var withConstructorImport = assembly.GetTypesWithConstructorImport();
            var withPropertyImport = assembly.GetTypesWithPropertiesImport();

            var withWrongAttributesUsage = withConstructorImport.Intersect(withPropertyImport).ToList();
            if (withWrongAttributesUsage.Any())
            {
                throw new ConventionException(
                    $"Both {typeof(ImportAttribute).FullName} and {typeof(ImportConstructorAttribute).FullName} cannot be used at the same time. " +
                    $"Types with wrongly defined import attributes: {string.Join(", ", withWrongAttributesUsage.Select(t => t.FullName))}");
            }

            var withExport = assembly.GetTypesWithTypeToExport();
            foreach (var typeAndTypeToResolve in withExport)
            {
                if (typeAndTypeToResolve.Value == null)
                {
                    AddType(typeAndTypeToResolve.Key);
                }
                else
                {
                    AddType(typeAndTypeToResolve.Key, typeAndTypeToResolve.Value);
                }
            }

            foreach (var type in withConstructorImport)
            {
                AddType(type, type, InjectionType.Constructor);
            }

            foreach (var type in withPropertyImport)
            {
                AddType(type, type, InjectionType.PropertySetter);
            }
        }

        public TToResolve CreateInstance<TToResolve>() => (TToResolve) CreateInstance(typeof(TToResolve));

        public object CreateInstance(Type typeToResolve)
        {
            var convention = _conventions.FirstOrDefault(o => o.TypeToResolve == typeToResolve);
            if (convention == null)
            {
                throw new TypeNotRegisteredException(typeToResolve);
            }

            var instance = GetInstanceWithDependencies(convention);
            return instance;
        }

        private object GetInstanceWithDependencies(RegisteredConvention convention)
        {
            switch (convention.Inject)
            {
                case InjectionType.Constructor:
                    return CreateAndInjectInConstructor(convention.ConcreteType);
                case InjectionType.PropertySetter:
                    return CreateAndInjectInProperties(convention.ConcreteType);
                default:
                {
                    throw new ConventionException($"Not supported injection type: {convention.Inject}");
                }
            }
        }

        private object CreateAndInjectInConstructor(Type typeToConstruct)
        {
            var constructorParameters = ResolveConstructorParameters(typeToConstruct).ToArray();
            if (constructorParameters.Length < 1) constructorParameters = null;

            return Activator.CreateInstance(typeToConstruct, constructorParameters);
        }

        private object CreateAndInjectInProperties(Type typeToConstruct)
        {
            var objectToResolve = Activator.CreateInstance(typeToConstruct);

            var properties = typeToConstruct.GetProperties()
                .Where(p => p.GetCustomAttribute<ImportAttribute>() != null);

            foreach (var property in properties)
            {
                property.SetValue(objectToResolve, CreateInstance(property.PropertyType));
            }

            return objectToResolve;
        }

        private IEnumerable<object> ResolveConstructorParameters(Type typeToResolve)
        {
            var constructorInfo = typeToResolve.GetConstructors().First();
            foreach (var parameter in constructorInfo.GetParameters())
            {
                yield return CreateInstance(parameter.ParameterType);
            }
        }
    }
}