using System;
using Module6.Container.Enums;
using Module6.Container.Exceptions;

namespace Module6.Container
{
    public class RegisteredConvention
    {
        public InjectionType Inject { get; internal set; }

        public Type TypeToResolve { get; internal set; }

        private Type _concreteType;

        public Type ConcreteType
        {
            get => _concreteType;
            internal set
            {
                AssertConcreteTypeCanBeInstantiated(value);
                _concreteType = value;
            }
        }

        public RegisteredConvention(Type concreteType, Type typeToResolve,
            InjectionType inject = InjectionType.Constructor)
        {
            AssertConcreteTypeCanBeInstantiated(concreteType);

            TypeToResolve = typeToResolve;
            ConcreteType = concreteType;
            Inject = inject;
        }

        public override string ToString() => $"{TypeToResolve}:{ConcreteType} [{Inject.ToString()}]";

        private static void AssertConcreteTypeCanBeInstantiated(Type concreteType)
        {
            if (concreteType.IsAbstract || concreteType.IsInterface)
            {
                throw new ConventionException(
                    $"Interface or abstract class cannot be a concrete type to resolve a dependency: {concreteType.FullName}");
            }
        }
    }
}