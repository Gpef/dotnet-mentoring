using System;
using System.Reflection;

namespace Module6.Container
{
    public interface IContainer
    {
        void AddType<TToResolve, TConcrete>();
        void AddType(Type typeToResolve);
        void AddType(Type typeToResolve, Type concreteType);
        void AddAssembly(Assembly assembly);
        object CreateInstance(Type typeToCreate);
        TToResolve CreateInstance<TToResolve>();
    }
}