﻿using Gtk;
using MultitargetLibrary;
using System;

namespace GtkApp.GTK
{
    class Program
    {
        static void Main()
        {
            Application.Init();
            MainWindow window = new MainWindow();
            Application.Run();
        }
    }

    public class MainWindow : Window
    {
        protected Entry NameEntry;
        protected Label HelloLabel;
        protected VBox VerticalBox;

        public MainWindow() : base("GtkHelloWorkdMain")
        {
            NameEntry = new Entry("Type in your name");
            HelloLabel = new Label("Hello!");
            VerticalBox = new VBox
            {
                NameEntry,
                HelloLabel
            };
            NameEntry.Changed += ChangeNameEvent;

            Add(VerticalBox);
            ShowAll();
        }

        protected void ChangeNameEvent(object sender, EventArgs args)
        {
            HelloLabel.Text = HelloWorld.GetHello(NameEntry.Text);
        }

        protected void OnDeleteEvent(object sender, DeleteEventArgs a)
        {
            Application.Quit();
        }
    }
}