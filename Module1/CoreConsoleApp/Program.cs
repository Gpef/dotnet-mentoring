﻿using MultitargetLibrary;
using System;

namespace CoreConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var name = args.Length > 0 ? args[0] : "DefaultUser";
            Console.WriteLine(HelloWorld.GetHello(name));
        }
    }
}
