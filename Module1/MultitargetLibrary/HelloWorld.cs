﻿using System;

namespace MultitargetLibrary
{
    public class HelloWorld
    {
        public static string GetHello(string name) =>
            $"{DateTime.Now.ToString("HH:mm")} Hello, {name}";
    }
}