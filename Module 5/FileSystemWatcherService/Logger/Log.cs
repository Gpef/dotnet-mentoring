using System;
using log4net;

namespace FileSystemWatcherService.Logger
{
    public static class Log
    {
        private static ILog _logger;
        private static ILog Logger => _logger ?? (_logger = Init());

        private static ILog Init()
        {
            var logger = LogManager.GetLogger("Logger");
            return logger;
        }

        public static void Debug(object message, Exception exception)
        {
            Logger.Debug(message, exception);
        }

        public static void Warn(object message, Exception exception)
        {
            Logger.Warn(message, exception);
        }

        public static void Info(object message, Exception exception)
        {
            Logger.Info(message, exception);
        }

        public static void Error(object message, Exception exception)
        {
            Logger.Error(message, exception);
        }

        public static void Debug(object message)
        {
            Logger.Debug(message);
        }

        public static void Warn(object message)
        {
            Logger.Warn(message);
        }

        public static void Info(object message)
        {
            Logger.Info(message);
        }

        public static void Error(object message)
        {
            Logger.Error(message);
        }
    }
}