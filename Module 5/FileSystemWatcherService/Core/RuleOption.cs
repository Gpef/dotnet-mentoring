﻿using System;

namespace FileSystemWatcherService.Core
{
    /// <summary>
    /// Specifies advanced options on file processing for <see cref="Rule"/>
    /// </summary>
    [Flags]
    public enum RuleOption
    {
        None = 0,
        /// <summary>
        /// If file moving date is need to be added to the filename.
        /// </summary>
        AddMoveDate = 1,
        /// <summary>
        /// If file's serial number is need to be added to the filename.
        /// </summary>
        AddSerialNumber = 2,
    }
}