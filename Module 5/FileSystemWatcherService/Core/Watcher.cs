using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions;
using System.Linq;
using System.Threading;
using FileSystemWatcherService.Configuration;
using FileSystemWatcherService.Logger;
using FileSystemWatcherService.Resources;

namespace FileSystemWatcherService.Core
{
    public class Watcher
    {
        private readonly IFileSystem _fileSystem;

        public IList<Rule> Rules => RulesDictionary.Values.ToList();
        private Dictionary<string, Rule> RulesDictionary { get; } = new Dictionary<string, Rule>();

        public IList<string> WatchingDirectories { get; } = new List<string>();
        public int MovedCount { get; private set; }

        private string _defaultDestinationDirectory;

        /// <summary>
        /// Returns or sets new Default path for moving files.
        /// </summary>
        /// <exception cref="ArgumentException">Provided path value is not an existing directory.</exception>
        public string DefaultDestinationDirectory
        {
            get => _defaultDestinationDirectory;
            set
            {
                if (!_fileSystem.Directory.Exists(value))
                {
                    var message = string.Format(R.PathNotExistForDefaultPath, value);
                    Log.Error(message);
                    throw new ArgumentException(message, nameof(value));
                }
                else
                {
                    _defaultDestinationDirectory = value;
                    Log.Info(string.Format("Default move path was successfully set to {0}", value));
                }
            }
        }

        private readonly Dictionary<string, FileSystemWatcherBase> _watchers =
            new Dictionary<string, FileSystemWatcherBase>();

        /// <summary>
        /// Creates instance of Watcher that watches specified directories and applies rules when moving them.
        /// </summary>
        /// <param name="fileSystem">File system in which watcher should work.</param>
        /// <param name="watchingDirectories">List of directories to setup watchers.</param>
        /// <param name="rules">List of rules to apply when new file appears in the watching directory.</param>
        /// <param name="defaultDestinationDirectory">Default directory to move files without matching rules.</param>
        /// <exception cref="ArgumentNullException">One of arguments is null.</exception>
        public Watcher(IFileSystem fileSystem, IList<string> watchingDirectories, IList<Rule> rules,
            string defaultDestinationDirectory)
        {
            if (rules == null) throw new ArgumentNullException(nameof(rules));
            if (defaultDestinationDirectory == null)
                throw new ArgumentNullException(nameof(defaultDestinationDirectory));
            if (watchingDirectories == null) throw new ArgumentNullException(nameof(watchingDirectories));
            if (fileSystem == null) throw new ArgumentNullException(nameof(fileSystem));

            _fileSystem = fileSystem;
            DefaultDestinationDirectory = defaultDestinationDirectory;

            RulesDictionary = new Dictionary<string, Rule>();
            foreach (var rule in rules)
            {
                AddRule(rule);
            }

            WatchingDirectories = watchingDirectories;
            foreach (var directory in watchingDirectories)
            {
                AddPath(directory);
            }
        }

        /// <summary>
        /// Creates instance of Watcher set up with provided config.
        /// </summary>
        /// <exception cref="ArgumentNullException">One of arguments is null.</exception>
        public Watcher(IFileSystem fileSystem, IWatcherConfiguration config)
        {
            if (null == config) throw new ArgumentNullException(nameof(config));
            if (fileSystem == null) throw new ArgumentNullException(nameof(config));

            this._fileSystem = fileSystem;
            Configure(config);
        }

        /// <summary>
        /// Adds to the watcher settings from provided config.
        /// </summary>
        /// <param name="config">Config to add new settings from.</param>
        public void Configure(IWatcherConfiguration config)
        {
            DefaultDestinationDirectory = config.DefaultDestination;

            foreach (var currentPath in config.Listen)
            {
                AddPath(currentPath);
            }

            foreach (var currentRuleConfiguration in config.Rules)
            {
                try
                {
                    var rule = new Rule(_fileSystem, currentRuleConfiguration);
                    AddRule(rule);
                }
                catch (Exception e)
                {
                    Log.Error(string.Format(R.RuleParsingError, currentRuleConfiguration.Pattern,
                        currentRuleConfiguration.Destination, e));
                }
            }
        }

        /// <summary>
        /// Adds specified rule to the rules list.
        /// </summary>
        /// <param name="rule">Rule to add</param>
        /// <exception cref="ArgumentNullException">One of arguments is null.</exception>
        public void AddRule(Rule rule)
        {
            if (rule == null) throw new ArgumentNullException(nameof(rule));

            RulesDictionary.Add(rule.FileNamePattern.ToString(), rule);
            Log.Info(string.Format(R.RuleAdded, rule.FileNamePattern.ToString(), rule.DestinationPath));
        }

        /// <summary>
        /// Removes specified rule from the rules list.
        /// </summary>
        /// <param name="rule">Rule to remove</param>
        /// <exception cref="ArgumentNullException">One of arguments is null.</exception>
        public void RemoveRule(Rule rule)
        {
            if (rule == null) throw new ArgumentNullException(nameof(rule));

            RulesDictionary.Remove(rule.FileNamePattern.ToString());
        }

        /// <summary>
        /// Add specified directory path to the watch list. If provided path is not an existing directory or 
        /// if this path already exists in the watch list, path will not be added.
        /// </summary>
        /// <param name="path">Directory path to add to the watch list</param>
        /// <exception cref="ArgumentNullException">One of arguments is null.</exception>
        public void AddPath(string path)
        {
            if (!_fileSystem.Directory.Exists(path))
            {
                Log.Info(string.Format(R.PathNotExistsForWatchList, path));
                return;
            }

            if (_watchers.ContainsKey(path))
            {
                Log.Info(string.Format(R.PathAlreadyExistInWatchList, path));
            }
            else
            {
                var watcher = _fileSystem.FileSystemWatcher.FromPath(path);
                watcher.IncludeSubdirectories = false;
                watcher.NotifyFilter = NotifyFilters.FileName;
                watcher.EnableRaisingEvents = true;
                watcher.Created += HandleCreated;

                _watchers.Add(path, watcher);
                Log.Info(string.Format(R.WatchAdded, path));
            }
        }

        /// <summary>
        /// Removes watch for the specified path if it is present in the watch list.
        /// </summary>
        /// <param name="path">Path to remove from the watch list.</param>
        /// <exception cref="ArgumentNullException">One of arguments is null.</exception>
        public void RemovePath(string path)
        {
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!_watchers.ContainsKey(path)) return;

            var fileSystemWatcher = _watchers[path];
            _watchers.Remove(path);
            fileSystemWatcher.Dispose();
        }

        /// <summary>
        /// Handles <see cref="FileSystemWatcher.OnCreated(FileSystemEventArgs)"/> event: finds a rule that macthes file created 
        /// and moves the file to the destination folder from the rule or default folder if there are no macthing rules.
        /// </summary>
        /// <param name="sender">Object that invoked the event.</param>
        /// <param name="args">Event arguments with info about file created.</param>
        /// <exception cref="ArgumentNullException">One of arguments is null.</exception>
        private void HandleCreated(object sender, FileSystemEventArgs args)
        {
            if (sender == null) throw new ArgumentNullException(nameof(sender));
            if (args == null) throw new ArgumentNullException(nameof(args));

            if (new DirectoryWrapper(_fileSystem).Exists(args.FullPath))
            {
                return;
            }

            Log.Info(string.Format(R.FileCreatedInWatchingDirectory, args.Name, DateTime.Now.ToString(Thread.CurrentThread.CurrentUICulture)));
            if (!_fileSystem.File.Exists(args.FullPath))
            {
                Log.Error(string.Format(R.FileNotExistAnymore, args.FullPath));
                return;
            }

            var destinationFullFileName = _fileSystem.Path.Combine(DefaultDestinationDirectory, args.Name);

            var rule = FirstMatchingRule(args.Name);
            if (null == rule)
            {
                Log.Info(string.Format(R.NoRulesFound, args.Name, DefaultDestinationDirectory));
            }
            else
            {
                Log.Info(string.Format(R.RuleFound, args.Name, rule.FileNamePattern, rule.DestinationPath));
                destinationFullFileName =
                    _fileSystem.Path.Combine(rule.DestinationPath, ProcessFileName(args.Name, rule));
            }

            try
            {
                _fileSystem.Directory.CreateDirectory(_fileSystem.Path.GetDirectoryName(destinationFullFileName));
                _fileSystem.File.Move(args.FullPath, destinationFullFileName);
            }
            catch (Exception e)
            {
                Log.Error(string.Format(R.ErrorMovingFile, e.Message));
            }
        }

        private string ProcessFileName(string oldFileName, Rule rule)
        {
            var serial = (rule != null) &&
                         ((rule.RuleOptions & RuleOption.AddSerialNumber) == RuleOption.AddSerialNumber)
                ? MovedCount + "-"
                : string.Empty;
            var date = (rule != null) && ((rule.RuleOptions & RuleOption.AddMoveDate) == RuleOption.AddMoveDate)
                ? DateTime.Now.ToString(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortDatePattern
                      .Replace(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.DateSeparator, "_")) + "-"
                : string.Empty;

            return $"{serial}{date}{oldFileName}";
        }

        private Rule FirstMatchingRule(string fileName) =>
            RulesDictionary
                .Where(currentRule => currentRule.Value.FileNamePattern.IsMatch(fileName))
                .Select(currentRule => currentRule.Value)
                .FirstOrDefault();
    }
}