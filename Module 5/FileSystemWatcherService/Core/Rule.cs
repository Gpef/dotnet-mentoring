﻿using System;
using System.IO;
using System.IO.Abstractions;
using System.Text.RegularExpressions;
using FileSystemWatcherService.Configuration;
using FileSystemWatcherService.Resources;

namespace FileSystemWatcherService.Core
{
    public class Rule
    {
        private readonly IFileSystem _fileSystem;
        
        public Regex FileNamePattern { get; }
        public string DestinationPath { get; }
        public RuleOption RuleOptions { get; private set; }

        /// <summary>
        /// Creates instance of Rule.
        /// </summary>
        /// <param name="fileSystem">File system the rule is applied to.</param>
        /// <param name="fileNamePattern">Regex pattern to match files.</param>
        /// <param name="destinationPath">Where to move files that match pattern.</param>
        /// <param name="ruleOptions">Additional options.</param>
        /// <exception cref="ArgumentException">A regular expression parsing error occured.</exception>
        /// <exception cref="DirectoryNotFoundException">Destination path is invalid.</exception>
        /// <exception cref="ArgumentNullException">One of arguments is null.</exception>
        public Rule(IFileSystem fileSystem, Regex fileNamePattern, string destinationPath, RuleOption ruleOptions = RuleOption.None)
        {
            if (destinationPath == null) throw new ArgumentNullException(nameof(destinationPath));
            if (fileSystem == null) throw new ArgumentNullException(nameof(fileSystem));

            _fileSystem = fileSystem;
            
            AssertPathExists(destinationPath);
            FileNamePattern = fileNamePattern ?? throw new ArgumentNullException(nameof(fileNamePattern));
            DestinationPath = destinationPath;
            RuleOptions = ruleOptions;
        }

        /// <inheritdoc />
        /// <summary>
        /// Creates instance of Rule.
        /// </summary>
        /// <param name="fileSystem">File system the rule is applied to.</param>
        /// <param name="config">Rule config that contains all necessary info about the rule.</param>
        public Rule(IFileSystem fileSystem, IRuleConfiguration config) : this(fileSystem, new Regex(config.Pattern), config.Destination)
        {
            if (config == null) throw new ArgumentNullException(nameof(config));

            if (config.IsMovingDateIncluded) AddOption(RuleOption.AddMoveDate);
            if (config.IsSerialNumberIncluded) AddOption(RuleOption.AddSerialNumber);
        }

        public void AddOption(RuleOption option)
        {
            RuleOptions |= option;
        }

        private void AssertPathExists(string path)
        {
            if (!_fileSystem.Directory.Exists(path))
            {
                try
                {
                    _fileSystem.Directory.CreateDirectory(path);
                }
                catch (Exception)
                {
                    throw new DirectoryNotFoundException(string.Format(R.RuleDirectoryNotExist, path));
                }
            };
        }
    }
}
