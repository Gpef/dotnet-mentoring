﻿using System;
using System.Globalization;
using System.IO.Abstractions;
using System.Text;
using System.Threading;
using FileSystemWatcherService.Configuration;
using FileSystemWatcherService.Core;
using FileSystemWatcherService.Logger;
using FileSystemWatcherService.Resources;

namespace FileSystemWatcherService
{
    public static class Program
    {
        private static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            var config = Config.GetAppConfig();
            SetCulture(config.Common?.Culture);

            if (null == config.Watcher)
            {
                var message = R.WatcherConfigNotFound;
                Log.Error(message);
                return;
            }

            var configuredWatcher = new Watcher(new FileSystem(), config.Watcher);

            Log.Info(R.WatchStarted);
            var cancellationToken = new CancellationTokenSource();
            Console.CancelKeyPress += (object sender, ConsoleCancelEventArgs e) =>
            {
                cancellationToken.Cancel();
            };

            cancellationToken.Token.WaitHandle.WaitOne();
        }

        private static void SetCulture(string culture)
        {
            if (string.IsNullOrEmpty(culture)) return;
            
            try
            {
                SetCulture(CultureInfo.GetCultureInfo(culture));
                Log.Info(string.Format(R.CultureApllied, culture));
            }
            catch (CultureNotFoundException)
            {
                SetCulture(CultureInfo.InvariantCulture);
                Log.Error(string.Format(R.CultureNotApplied, culture));
            }
        }

        private static void SetCulture(CultureInfo culture)
        {
            Thread.CurrentThread.CurrentUICulture = culture;
            Thread.CurrentThread.CurrentCulture = culture;
            CultureInfo.DefaultThreadCurrentCulture = culture;
            CultureInfo.DefaultThreadCurrentUICulture = culture;
        }
    }
}