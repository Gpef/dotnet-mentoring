﻿using System.Collections.Generic;

namespace FileSystemWatcherService.Configuration
{
    public interface IWatcherConfiguration
    {
        IEnumerable<string> Listen { get; }
        IEnumerable<IRuleConfiguration> Rules { get; }
        string DefaultDestination { get; }
    }
}
