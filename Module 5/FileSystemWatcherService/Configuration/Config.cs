﻿using Nerdle.AutoConfig;
using System.Collections.Generic;

namespace FileSystemWatcherService.Configuration
{
    public class Config
    {
        private const string MatchAllPattern = ".*";
        private const bool RuleOptionsDefaultValue = false;
        private const string DefaultPathValue = "";

        public static IAppConfiguration GetAppConfig()
        {
            AutoConfig.WhenMapping<IRuleConfiguration>(mapping =>
            {
                mapping.Map(x => x.IsMovingDateIncluded).OptionalWithDefault(RuleOptionsDefaultValue);
                mapping.Map(x => x.IsSerialNumberIncluded).OptionalWithDefault(RuleOptionsDefaultValue);
                mapping.Map(x => x.Pattern).OptionalWithDefault(MatchAllPattern);
            });

            AutoConfig.WhenMapping<IWatcherConfiguration>(mapping =>
            {
                mapping.Map(x => x.Listen).OptionalWithDefault(new List<string>());
                mapping.Map(x => x.Rules).OptionalWithDefault(new List<IRuleConfiguration>());
                mapping.Map(x => x.DefaultDestination).OptionalWithDefault(DefaultPathValue);
            });

            AutoConfig.WhenMapping<ICommonConfiguration>(mapping =>
            {
                mapping.Map(x => x.Culture).Optional();
            });

            AutoConfig.WhenMapping<IAppConfiguration>(mapping =>
            {
                mapping.Map(x => x.Common).Optional();
                mapping.Map(x => x.Watcher).Optional();
            });

            return AutoConfig.Map<IAppConfiguration>();
        }
    }
}