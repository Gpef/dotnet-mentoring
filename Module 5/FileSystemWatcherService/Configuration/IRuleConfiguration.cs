﻿namespace FileSystemWatcherService.Configuration
{
    public interface IRuleConfiguration
    {
        string Pattern { get; }
        string Destination { get; }
        bool IsSerialNumberIncluded { get; } 
        bool IsMovingDateIncluded { get; } 
    }
}