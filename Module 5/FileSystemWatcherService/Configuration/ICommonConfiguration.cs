﻿namespace FileSystemWatcherService.Configuration
{
    public interface ICommonConfiguration
    {
        string Culture { get; }
    }
}
