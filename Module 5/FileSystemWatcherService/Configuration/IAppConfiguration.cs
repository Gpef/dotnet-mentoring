﻿namespace FileSystemWatcherService.Configuration
{
    public interface IAppConfiguration
    {
        ICommonConfiguration Common { get; }
        IWatcherConfiguration Watcher { get; }
    }
}
